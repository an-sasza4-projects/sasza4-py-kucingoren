from hashlib import sha256
from pathlib import Path
import pytest
from kucingoren.gameio import csvfile
from kucingoren.gameio.csvfile import (
    gamecfg as csvgamecfg
)
from kucingoren.util.serializer import LeastTwoPrecisionDigitsFloat as float2p
from . import TEMP_DIR, iter_lazy, fp_sha256



CSV_GAMECFG = csvgamecfg.load( Path("./tests/resources/cfg.csv").open("rb+") )


def _load (inval: csvgamecfg.GameConfigDict):
    assert len(inval) == 187
    CSV_GAMECFG_VALS = {
        "localization.dialogMessage.onslaughtMessages.message1.duration": 5
        , "localization.dialogMessage.onslaughtMessages.message2.duration": 5
        , "localization.combotMessage.firstStrongboxPickup.icon": "ACTIVE_CHARACTER"
        , "localization.combotMessage.useZombieAntidote.icon": "MEDIC"
        , "game.skills.base.0.passive": True
        , "game.skills.base.3.diminishing": False
        , "game.skills.base.3.amount": float2p(0.10)
        , "game.enemy.zombieAttributes": "// Percentage increase of bass stats E.g. 1.35 is 35% increase, 0.50 is 50% decrease"
        , "game.enemy.zombieAttributes.zombie": "// Base Attributes + [Num] // Not additive ontop of prior tiers, manually set each type individually"
        , "game.enemy.zombieAttributes.boss.meleeDamage.damageIncreaseEliteBoss": float2p(1.50)
        , "game.enemy.zombieAttributes.boss.meleeDamage.damageIncreaseNightmareBoss": 2.0
        , "game.liveOps.gameModes.0.balance.difficultyPerSecondNoVirus": float2p(-0.40)
        , "game.liveOps.gameModes.0.balance.difficultyChangeOnCarrierDeath": -2.0
        , "game.end": ""
    }
    # KV check.
    for k,v in CSV_GAMECFG_VALS.items():
        assert inval.get(k) == v
    # Serialize stricting.
    if True:#Make its own local-scoped block.
        out_csv = inval.to_csv( strict=False )
        for k,v in CSV_GAMECFG_VALS.items():
            in_cfgval = v
            if isinstance( v, float2p ):
                assert (k, csvgamecfg._serialize(k,v,)[-1]) not in out_csv
                in_cfgval = float(in_cfgval)
            assert (k, csvfile._serialize(in_cfgval)) in out_csv
        out_csv = inval.to_csv( strict=True )
        for k,v in CSV_GAMECFG_VALS.items():
            if isinstance( v, float2p ):
                assert (k, csvfile._serialize(float(v))) not in out_csv
            assert (k, csvgamecfg._serialize(k,v,)[-1]) in out_csv
    # Existent keys.
    for ref in CSV_GAMECFG_VALS.items():
        assert inval.find(ref[0])[1] == ref[1]
        assert inval.find(ref[0], multi_value=True)[0][1] == ref[1]
        assert inval.find(ref[0], offset=0)[1] == ref[1]
        assert inval.find(ref[0], offset=0, multi_value=True)[0][1] == ref[1]
    if True:#Make its own local-scoped block.
        out_iter = iter( CSV_GAMECFG_VALS.items() )
        out_1st = next( out_iter )
        out_2nd = next( out_iter )
        in_key = out_1st[1]
        assert inval.find(in_key)[0] == out_1st[0]
        out_mulval = inval.find(in_key, multi_value=True)
        assert out_mulval[0] == out_1st
        assert out_mulval[1] == out_2nd
        assert inval.find(in_key, offset=1)[0] == out_1st[0]
        out_mulval = inval.find(in_key, offset=1, multi_value=True)
        assert out_mulval[0] == out_1st
        assert out_mulval[1] == out_2nd
    # Non-existent keys.
    assert inval.find("n0t f0und") is None
    assert inval.find("n0t f0und", multi_value=True) is None
    assert inval.find("n0t f0und", offset=1) is None
    assert inval.find("n0t f0und", offset=1, multi_value=True) is None
    assert inval.find("n0t f0und", default=False) == False
    assert inval.find("n0t f0und", default=False, multi_value=True) == False
    assert inval.find("n0t f0und", default=False, offset=1) == False
    assert inval.find("n0t f0und", default=False, offset=1, multi_value=True) == False


def test_load ():
    for x in iter_lazy( CSV_GAMECFG, (lambda:csvgamecfg.loads(csvgamecfg.dumps(CSV_GAMECFG))) ):
        _load(x)


def test_dump ():
    expected_hash = '39711c32140a0bbbaa894e4cf01f56e205c9d85913937ad3a528b7e34f7e65a5'
    with (TEMP_DIR/"test_csvgamecfg_dump.csv").open("wb+") as fp:
        csvgamecfg.dump( CSV_GAMECFG, fp, strict=True )
        fp.seek( 0 )
        assert fp_sha256( fp ) == expected_hash
    assert sha256(csvgamecfg.dumps(CSV_GAMECFG, strict=True).encode('utf8')).hexdigest() == expected_hash

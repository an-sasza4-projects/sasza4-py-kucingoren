from hashlib import sha256
from pathlib import Path
import pytest
from kucingoren.gameio import csvfile
from . import TEMP_DIR, iter_lazy, fp_sha256



CSV_FILEPATH = Path( "./tests/resources/lang.csv" )
CSV_VAL0 = ("1233", "weapon.Trailblazer_v1.name", "Trailblazer")
CSV_VAL310 = ("0", "playerClass.class.heavy.level.100", "High Gunnery Commander")


CSV = csvfile.load( CSV_FILEPATH.open("rb+") )
def _load (inval: csvfile.CSV):
    assert len(inval) == 311
    _load_extra( inval )
def _load_extra (inval: csvfile.CSV, i310 = 310):
    # Existent keys.
    assert inval[0] == CSV_VAL0
    assert inval.find("1233") == CSV_VAL0
    assert inval.find("1233", multi_value=True)[0] == CSV_VAL0
    assert inval.find("1233", offset=0) == CSV_VAL0
    assert inval.find("1233", offset=0, multi_value=True)[0] == CSV_VAL0
    assert inval[i310] == CSV_VAL310
    assert inval.find("playerClass.class.heavy.level.100") == CSV_VAL310
    assert inval.find("playerClass.class.heavy.level.100", multi_value=True)[0] == CSV_VAL310
    assert inval.find("playerClass.class.heavy.level.100", offset=1) == CSV_VAL310
    assert inval.find("playerClass.class.heavy.level.100", offset=1, multi_value=True)[0] == CSV_VAL310
    assert inval.find("0") != CSV_VAL310
    assert inval.find("0", multi_value=True)[-1] == CSV_VAL310
    assert inval.find("0", offset=0) != CSV_VAL310
    assert inval.find("0", offset=0, multi_value=True)[-1] == CSV_VAL310
    # Non-existent keys.
    assert inval.find("n0t f0und") is None
    assert inval.find("n0t f0und", multi_value=True) is None
    assert inval.find("n0t f0und", offset=1) is None
    assert inval.find("n0t f0und", offset=1, multi_value=True) is None
    assert inval.find("n0t f0und", default=False) == False
    assert inval.find("n0t f0und", default=False, multi_value=True) == False
    assert inval.find("n0t f0und", default=False, offset=1) == False
    assert inval.find("n0t f0und", default=False, offset=1, multi_value=True) == False
    with pytest.raises( IndexError ):
        inval.find( "1233", offset=133337 )


def test_load ():
    for x in iter_lazy( CSV, (lambda:csvfile.loads(csvfile.dumps(CSV))) ):
        _load(x)


def test_load_abnormal ():
    inval = csvfile.load( Path("./tests/resources/abnormal.csv").open("rb+") )
    assert len(inval) == 17
    _load_extra( inval, 16 )


def test_dump ():
    expected_hash = 'f88b2b69973ac5ea80c4b9f84f8480507853331fddb1896256d8ddac27f9810b'
    with (TEMP_DIR/"test_csv_dump.csv").open("wb+") as fp:
        csvfile.dump( CSV, fp )
        fp.seek( 0 )
        assert fp_sha256( fp ) == expected_hash
    assert sha256(csvfile.dumps(CSV).encode('utf8')).hexdigest() == expected_hash

from kucingoren import profile



def test_default ():
    assert profile.Profile().Version.LastGame == (1,9,1)


def test_dict ():
    inval = {
        "Version":{
            "LastGame":[
                1,
                9,
                2
            ],
        },
    }
    assert profile.Profile.from_dict( inval ).Version.LastGame == (1,9,2)

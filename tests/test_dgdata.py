from hashlib import sha256
from pathlib import Path
import pytest
from kucingoren.gameio import dgdata
from . import TEMP_DIR, fp_sha256



BAD_SAMPLES = [
    "./tests/resources/checksum_mismatch.dgdata"
    , "./tests/resources/bad_header.dgdata"
    , "./tests/resources/bad_checksum.dgdata"
    , "./tests/resources/bad_checksum2.dgdata"
]
GOOD_SAMPLES = [
    ("./tests/resources/valid.dgdata", "78564523ecdccbd8868d4468a9140f1c5931c146eaeb8a809fcc943683bbc2f6",)
    , ("./tests/resources/empty.dgdata", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",)
]
RAW_SAMPLES = [
    (b"foobarbaz", b"DGDATA080a153d{\x85\x86zz\x8cww\x91",)
    , (b"", b"DGDATA00000000",)
]


def test_decoder_fromfile_invalid ():
    for filepath in BAD_SAMPLES:
        with Path(filepath).open("rb+") as fp:
            if filepath == BAD_SAMPLES[0]:
                assert dgdata.get_checksum(fp.read(dgdata.HEADER_SIZE)) == b'baaaaaad'
                fp.seek( 0 )
            with pytest.raises( dgdata.DGDataDecodeError ):
                for _ in dgdata.iter_decode_fromfile( fp, verify=True ):
                    pass


def test_decoder_fromfile_valid ():
    for i, (filepath, expected_hash) in enumerate(GOOD_SAMPLES):
        with Path(filepath).open("rb+") as fp:
            retval = []
            out_hash = sha256()
            for x in dgdata.iter_decode_fromfile( fp, verify=True ):
                out_hash.update( x )
                retval.append( x )
            assert out_hash.hexdigest() == expected_hash
            if i == 1:
                assert len(retval) == 0


def test_decoder_frombuffer ():
    for decoded, encoded in RAW_SAMPLES:
        retval = b''.join( dgdata.iter_decode_frombuffer(encoded) )
        assert retval == decoded


def test_decoder_tofile ():
    for decoded, encoded in RAW_SAMPLES:
        with (TEMP_DIR/"test_decoder_tofile.dgdata").open("wb+") as out_fp:
            dgdata.decode_tofile( encoded, out_fp )
            out_fp.seek( 0 )
            assert out_fp.read() == decoded


def test_encoder_fromfile ():
    expected_hash = '0887eecd17663a06c81e9861834ba5f6c62b833741348858244520ef72d28647'
    with Path("./tests/resources/valid.json").open("rb+") as in_fp:
        with (TEMP_DIR/"test_encoder_fromfile.dgdata").open("wb+") as out_fp:
            dgdata.encode_readwrite_helper( out_fp, in_fp )
            out_fp.seek( 0 )
            assert fp_sha256( out_fp ) == expected_hash


def test_encoder_frombuffer ():
    for decoded, encoded in RAW_SAMPLES:
        retval = b''.join( dgdata.iter_encode_frombuffer(decoded) )
        assert retval == encoded


def test_encoder_tofile ():
    for decoded, encoded in RAW_SAMPLES:
        with (TEMP_DIR/"test_encoder_tofile.dgdata").open("wb+") as out_fp:
            dgdata.encode_tofile( decoded, out_fp )
            out_fp.seek( 0 )
            assert out_fp.read() == encoded

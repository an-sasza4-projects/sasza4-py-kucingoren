from typing import TypeVar, Callable, Iterator
from hashlib import sha256
from pathlib import Path



T = TypeVar("T")
TEMP_DIR = Path( "./.tmp" )
if not TEMP_DIR.exists():
    TEMP_DIR.mkdir( parents=True )


def iter_lazy (*args: T) -> Iterator[T]:
    for x in args:
        if isinstance( x, Callable ):
            yield x()
            continue
        yield x


def fp_sha256 (fp) -> str:
    hashing = sha256()
    while buf := fp.read(4096):
        hashing.update(buf)
    return hashing.hexdigest()

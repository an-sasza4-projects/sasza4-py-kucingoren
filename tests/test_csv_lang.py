from hashlib import sha256
from pathlib import Path
import pytest
from kucingoren.gameio.csvfile import (
    lang as csvlang
)
from . import TEMP_DIR, iter_lazy, fp_sha256



CSV_LANG_FILEPATH = Path( "./tests/resources/lang.csv" )
CSV_LANG_VAL0 = ("1233", "weapon.Trailblazer_v1.name", "Trailblazer")
CSV_LANG_VAL310 = ("0", "playerClass.class.heavy.level.100", "High Gunnery Commander")


CSV_LANG = csvlang.load( CSV_LANG_FILEPATH.open("rb+") )
def _load (inval: csvlang.LanguageDict):
    assert len(inval) == 311
    # Existent keys.
    assert inval["1233"] == CSV_LANG_VAL0
    assert inval.find(1233) == CSV_LANG_VAL0
    assert inval.find(1233, multi_value=True)[0] == CSV_LANG_VAL0
    with pytest.raises( TypeError ):
        assert inval.find(1233, offset=0) == CSV_LANG_VAL0
    with pytest.raises( TypeError ):
        assert inval.find(1233, offset=0, multi_value=True)[0] == CSV_LANG_VAL0
    assert inval.find("playerClass.class.heavy.level.100") == CSV_LANG_VAL310
    assert inval.find("playerClass.class.heavy.level.100", multi_value=True)[0] == CSV_LANG_VAL310
    assert inval.find("playerClass.class.heavy.level.100", offset=1) == CSV_LANG_VAL310
    assert inval.find("playerClass.class.heavy.level.100", offset=1, multi_value=True)[0] == CSV_LANG_VAL310
    assert inval.find(0) != CSV_LANG_VAL310
    assert inval.find(0, multi_value=True)[-1] == CSV_LANG_VAL310
    # Non-existent keys.
    assert inval.find(1234567890) is None
    assert inval.find(1234567890, multi_value=True) is None
    assert inval.find("n0t f0und") is None
    assert inval.find("n0t f0und", multi_value=True) is None
    assert inval.find("n0t f0und", offset=1) is None
    assert inval.find("n0t f0und", offset=1, multi_value=True) is None
    assert inval.find(1234567890, default=False) == False
    assert inval.find(1234567890, default=False, multi_value=True) == False
    assert inval.find("n0t f0und", default=False) == False
    assert inval.find("n0t f0und", default=False, multi_value=True) == False
    assert inval.find("n0t f0und", default=False, offset=1) == False
    assert inval.find("n0t f0und", default=False, offset=1, multi_value=True) == False


def test_load ():
    for x in iter_lazy( CSV_LANG, (lambda:csvlang.loads(csvlang.dumps(CSV_LANG))) ):
        _load(x)


def test_dump ():
    expected_hash = 'f88b2b69973ac5ea80c4b9f84f8480507853331fddb1896256d8ddac27f9810b'
    with (TEMP_DIR/"test_csvlang_dump.csv").open("wb+") as fp:
        csvlang.dump( CSV_LANG, fp )
        fp.seek( 0 )
        assert fp_sha256( fp ) == expected_hash
    assert sha256(csvlang.dumps(CSV_LANG).encode('utf8')).hexdigest() == expected_hash

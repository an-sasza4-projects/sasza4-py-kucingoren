from kucingoren.util import serializer



def test_float ():
    assert type( serializer.serialize_f(1) ) == int
    assert type( serializer.serialize_f(1.0) ) == float
    assert serializer.serialize_f(1.0) == 1.0
    assert serializer.serialize_f(1.111) == 1.111


def test_float2p ():
    assert type( serializer.serialize_f2p(1) ) == int
    assert type( serializer.serialize_f2p(1.0) ) == serializer.LeastTwoPrecisionDigitsFloat
    assert serializer.serialize_f2p(1.0) == 1.0
    assert str( serializer.serialize_f2p(1.0) ) == "1.00"
    assert serializer.serialize_f2p(1.111) == 1.111
    assert str( serializer.serialize_f2p(1.111) ) == "1.111"

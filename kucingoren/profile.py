"""
Player Profile module.
"""
from __future__ import annotations
import typing as _typing
import dataclasses as _dc
from .gameio import Version
from .util.dataclasses_ import (
    ABCDictDataclass as _DictDataclass
)



@_dc.dataclass
class VersionData (_DictDataclass):
    """Profile version data."""

    LastGame: Version = _dc.field( default_factory=(lambda:Version(1,9,1)) )
    """Last game version that the profile was loaded in."""

    OriginalVersion: Version = _dc.field( default_factory=(lambda:Version(1,9,1)) )
    """Profile was created on this game version."""

    Profile: Version = _dc.field( default_factory=(lambda:Version(1,0,0)) )
    """Profile version."""

    SaveTime: int = 1508153479
    """Last save time in Unix timestamp."""

    link: str = None
    """Linked account ID."""

    analytics: str = None
    """Unknown. For analytics purposes."""


@_dc.dataclass
class SettingsData (_DictDataclass):
    """Profile settings data."""

    Name: str = None
    """Player name."""

    ScreenShakeEnabled: bool = True
    """Explosion impact and screen movement."""

    MusicOn: bool = True
    """Menu & in-game music."""

    SFXOn: bool = True
    """Menu & in-game sound effects."""

    DeadZone: bool = False
    """Adds a buffer to right analouge stick to allow aiming before shooting."""

    TriggerFire: bool = False
    """Use gamepad trigger to fire."""

    VSync: bool = True
    """V-Sync."""

    HudControls: bool = True
    """Display HUD control icons."""

    SkipIntroScenes: bool = True
    """Skips pre-mission intro scenes."""

    CheckedForAccounts: bool = False
    """Unknown."""

    PreventAutoSignIn: bool = False
    """Unknown."""

    # Version >= 2.X.X
    UseSingleStickDriving: bool = None
    """Unknown."""

    UseFlipSticksDriving: bool = None
    """Unknown."""


@_dc.dataclass
class PurchasedIAPItem (_DictDataclass):
    """Purchased in-app purchase item."""

    Identifier: str
    """Item ID."""

    Value: bool = False
    """Purchase status."""


@_dc.dataclass
class IAPData (_DictDataclass):
    """Profile in-app purchase data."""

    PurchasedIAP: _typing.Dict = _dc.field( default_factory=(lambda: {"PurchasedIAPArray":list()}) )
    """Purchased in-app purchases."""

    PendingIapArray: _typing.List = _dc.field( default_factory=list )
    """Pending in-app purchases."""


@_dc.dataclass
class MiscRewardData (_DictDataclass):
    """Profile miscellaneous reward data."""

    GiftArray: _typing.List = _dc.field( default_factory=list )
    """List of gifts."""

    CoopEventRewards: _typing.List[str] = _dc.field( default_factory=list )
    """List of co-op event rewards."""


@_dc.dataclass
class GlobalData (_DictDataclass):
    """Profile shared data."""

    HighestRank: int = 0
    """Highest rank achieved by this profile."""

    HasPlayedGame: bool = False
    """Player had played the game at least once."""

    ForceGiveArmour: bool = True
    """Give the starter equipments."""

    ReviveTokens: int = 3
    """Revive token that player has for multiplayer games."""

    TimeOfLastUpload: int = 0
    """Last profile upload time in Unix timestamp."""

    ForceRemoveAds: bool = False
    """Remove the in-game advertisements."""

    AvailablePremiumTickets: int = 0
    """Premium ticket that player has for nightmare-mode games."""


@_dc.dataclass
class BaseProfile (_DictDataclass):
    Version: VersionData = _dc.field( default_factory=VersionData )
    """Version data."""

    Settings: SettingsData = _dc.field( default_factory=SettingsData )
    """Game settings data."""

    UsingJazzyZoom: bool = True
    """Allow camera close-up-zoom effect when player is idling."""

    ShowGore: bool = True
    """Display gibs, splats, and corpses."""

    Global: GlobalData = _dc.field( default_factory=GlobalData )
    """Profile global data."""

    Achievements: _typing.List[str] = _dc.field( default_factory=list )
    """List of player achievements."""

    GenericData: _typing.List = _dc.field( default_factory=list )
    """List of generic data."""

    HackCheck: int = 0
    """Hack & cheat detection bitflag."""


@_dc.dataclass
class Profile (MiscRewardData, IAPData, BaseProfile):
    """User profile."""

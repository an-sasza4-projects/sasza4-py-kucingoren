"""
Game data structures & I/O utility module.
"""
from __future__ import annotations
import typing as _typing
import win32api as _wapi
import steamid_converter.Converter as _sidcvt
from pathlib import (
    Path as _Path
)
from .csvfile import (
    lang as _csvlang
    , gamecfg as _csvgamecfg
)
from . import (
    steam as _st
    , jetfile as _jet
)



SASZA4_GAME_PATH = _st.STEAMAPPS_DIR + "/common/SAS Zombie Assault 4/"
SASZA4_GAME_EXE_FILENAME = "SAS4-Win.exe"
SASZA4_PROFILE_PATH = "local/Data/Docs/"
SASZA4_PROFILE_FILENAME = "Profile.save"
SASZA4_JET_DATA_PATH = "Assets/data.jet"
SASZA4_CSV_LANGUAGE_PATH = "Assets/GameData/newEnglish.csv"
SASZA4_CSV_CONFIG_PATH = "Assets/GameData/config.csv"


class Version (_typing.NamedTuple):
    """Semantic versioning."""
    major: int
    minor: int
    patch: int


def get_game_path ():
    """Return SAS:ZA4 game app path."""
    libpath = _st.get_steam_library_path()
    if not libpath:
        return None
    return (libpath / SASZA4_GAME_PATH).resolve( strict=True )


def iter_get_game_save_paths (
    steamid: str|int
) -> _typing.Iterator[_Path]:
    """
    Return any `Profile.save` file paths found within given Steam ID.

    Parameters
    ----------
    steamid : str|int
        steamID or steamID64 that has SAS:ZA4 save game.
    """
    userid = _sidcvt.to_steamID3( steamid ).strip( "[]" ).split( ":", 2 )[-1]
    profpath = (
        _st.get_steam_path()
        / _st.USERDATA_DIR
        / userid
        / _st.STEAMAPPS_SASZA4_ID
        / SASZA4_PROFILE_PATH
    ).resolve( strict=True )
    # Hash-like folder.
    for x in profpath.iterdir():
        if not x.is_dir():
            continue
        if len( x.name ) < 24:
            continue
        retval = (x / SASZA4_PROFILE_FILENAME)
        if not retval.exists():
            continue
        yield retval
    # Docs folder.
    retval = (profpath / SASZA4_PROFILE_FILENAME)
    if retval.exists():
        yield retval


def get_game_save_path (
    steamid: str|int
) -> _Path|None:
    """
    Return the first result of `iter_get_game_save_paths`.
    Otherwise, return `None`.

    Parameters
    ----------
    steamid : str|int
        steamID or steamID64 that has SAS:ZA4 save game.
    """
    try:
        return next( iter_get_game_save_paths(steamid) )
    except StopIteration:
        return None


def get_game_app_manifest () -> dict[str, _typing.Any]:
    """Return SAS:ZA4 game app manifest."""
    gamepath = get_game_path()
    if not gamepath:
        raise ValueError( "Game path is not found" )
    return _wapi.GetFileVersionInfo(
        str((gamepath/SASZA4_GAME_EXE_FILENAME).resolve(strict=True))
        , "\\"
    )


def get_game_app_version () -> Version:
    """Return SAS:ZA4 game app version."""
    manifest = get_game_app_manifest()
    ms = manifest['FileVersionMS']
    ls = manifest['FileVersionLS']
    # X.X.X
    return Version(
        (ms >> 16) & 0xFFFF
        , ms & 0xFFFF
        , (ls >> 16) & 0xFFFF
    )


def get_game_englishcsv ():
    """Return new instance of "newEnglish.csv" `LanguageDict` object."""
    gamepath = get_game_path()
    if not gamepath:
        raise ValueError( "Game path is not found" )
    filepath = (gamepath / SASZA4_CSV_LANGUAGE_PATH).resolve( strict=True )
    with filepath.open( "rb" ) as fd:
        return _csvlang.load( fd )


def get_game_datajet (
    *args
    , **kwargs
):
    """Return new instance of "data.jet" `JetFile` object."""
    gamepath = get_game_path()
    if not gamepath:
        raise ValueError( "Game path is not found" )
    filepath = (gamepath / SASZA4_JET_DATA_PATH).resolve( strict=True )
    return _jet.JetFile( filepath, *args, **kwargs )


def get_game_configcsv ():
    """Return new instance of "config.csv" `GameConfigDict` object."""
    with get_game_datajet().open(SASZA4_CSV_CONFIG_PATH) as fd:
        return _csvgamecfg.load( fd )

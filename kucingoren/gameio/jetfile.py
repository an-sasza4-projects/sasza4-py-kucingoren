"""
JET archive module.
"""
from __future__ import annotations
import typing as _typing
import struct as _struct
import codecs7z as _c7z
from zipfile import (
    ZIP_STORED as _ZIP_STORED
    , ZIP_DEFLATED as _ZIP_DEFLATED
)
from ..util.zipfile_ import (
    _ZipFileMode as _ZipFileMode
    , _StrPath as _StrPath
    , _structFileHeader as _structFileHeader
    , _sizeFileHeader as _sizeFileHeader
    , _MASK_ENCRYPTED as _MASK_ENCRYPTED
    , _ZipInfo as _ZipInfo
)
from ..util.zipfile_.pkzip import (
    to_pkzipinfo as _zi2pkzi
    , read_encryption_key as _get_encryption_key
    , PKZipFile as _PKZipFile
)



PASSWORD = b"Q%_{6#Px]]"
"""
Common password for JET archive.

Courtesy of Luigi Auriemma.
https://aluigi.altervista.org/papers.htm#info
"""


class JetFile (_PKZipFile):
    """
    A generic PKZIP 2.x, usually with Deflate+Store compression.

    Deflate compression will be using 7-Zip (`codecs7z` module)'s Deflate algorithm.
    """
    @classmethod
    def get_compressor (cls, compress_type: int, compresslevel: int|None = None):
        if compress_type == _ZIP_DEFLATED:
            if compresslevel is None:
                compresslevel = -1
            return _c7z.deflate_compressobj( compresslevel )
        return super(JetFile, cls).get_compressor( compress_type, compresslevel )

    @classmethod
    def edit_local_fileheader (cls, fileheader: bytes) -> bytes:
        # Strip the extra bytes to match with 7-Zip's zip creation.
        header_raw = fileheader[:_sizeFileHeader]
        header = list(_struct.unpack( _structFileHeader, header_raw ))
        header[11] = 0 #extra_len
        filename_len = header[10]
        filename = fileheader[_sizeFileHeader:_sizeFileHeader+filename_len]
        header_new_raw = _struct.pack( _structFileHeader, *header )
        return header_new_raw + filename

    def on_file_overwrite (self, zinfo_new: _ZipInfo, zinfo_old: _ZipInfo) -> _ZipInfo:
        zinfo_new = super(JetFile, self).on_file_overwrite( zinfo_new, zinfo_old )
        zinfo_new = _zi2pkzi( zinfo_new, ref=zinfo_old, copy_key=True )
        return zinfo_new

    def __init__ (
        self
        , file: _StrPath
        , mode: _ZipFileMode = "r"
        , compression: int = _ZIP_STORED
        , allowZip64: bool = False
        , compresslevel: int|None = None
        , pwd: bytes|_typing.Literal[False]|None = None
        , **kwargs
    ) -> None:
        super(JetFile, self).__init__(
            file, mode, compression, allowZip64, compresslevel
            , **kwargs
        )
        if pwd is None:
            pwd = PASSWORD
        if pwd:
            self.setpassword( pwd )
            self.fix_fileinfo_encryption_keys()

    def fix_fileinfo_encryption_keys (
        self
        , pwd:bytes|None = None
        , ignore_invalid_pwd:bool|None = None
    ) -> bool:
        """
        Fix missing `encryption_first_key` property for entire
        file info of this `JetFile` by trying to decrypt the
        encryption header with given `pwd` password bytes.

        Parameters
        ----------
        pwd: bytes|None
            Decryption password.
            `None` goes fallback to default password.
        ignore_invalid_pwd: bool|None
            Silently ignoring wrong password exceptions, if set `True`.
            Default is `True`.

        Returns
        ----------
        - `True`:
            Successfully repaired.
        - `False`:
            This `ZipFile` is not seekable.
        """
        if not self._seekable:
            return False
        pwd = self._get_pwd( pwd )
        if pwd and not isinstance(pwd, bytes):
            raise TypeError("pwd: expected bytes, got %s" % type(pwd).__name__)
        if ignore_invalid_pwd is None:
            ignore_invalid_pwd = True
        for zinfo_old in self.filelist:
            if (zinfo_old.flag_bits & _MASK_ENCRYPTED):
                if not pwd:
                    raise RuntimeError("File %r is encrypted, password "
                                        "required for extraction" % zinfo_old.filename)
                # Copy the first 11 bytes of source encryption header.
                # Zip file has to be seekable.
                try:
                    first_key = _get_encryption_key( self, zinfo_old, pwd )
                except RuntimeError:
                    if ignore_invalid_pwd:
                        pass
                    raise
                else:
                    zinfo_new = _zi2pkzi( zinfo_old )
                    zinfo_new.encryption_first_key = first_key
                    self._replace_fileinfo( zinfo_new, zinfo_old )
        return True

"""
Steam utility module.
"""
from __future__ import annotations
import typing as _typing
from pathlib import (
    Path as _Path
)
from os import (
    environ as _environ
)
import win32con as _wcon
import win32api as _wapi
import vdf as _vdf



USERDATA_DIR = "userdata"
STEAMAPPS_DIR = "steamapps"
STEAMAPPS_SASZA4_ID = "678800"


def get_steam_path () -> _Path:
    """
    Return Steam program files path retrieved from environment variable or Windows Registry.
    """
    envval = _environ.get( "STEAM_PATH" )
    if envval:
        return _Path( envval ).resolve( strict=True )
    regdir = _wapi.RegOpenKeyEx( _wcon.HKEY_CURRENT_USER, "Software\\Valve\\Steam" )
    regval,_ = _wapi.RegQueryValueEx( regdir, "SteamPath" )
    return _Path( regval ).resolve( strict=True )


def get_steam_library_path () -> _Path|None:
    """
    Return Steam library path that has SAS:ZA4 game installed.
    """
    with (get_steam_path() / "steamapps" / "libraryfolders.vdf").open('r') as fp:
        lib: dict[str,_typing.Any] = _vdf.parse( fp )
        lib = lib["libraryfolders"]
        for v in lib.values():
            if not isinstance( v, dict ):
                continue
            v_apps: dict[str,_typing.Any]|None = v.get( "apps" )
            if not isinstance( v_apps, dict ):
                continue
            v_apps_sasza4: str|None = v_apps.get( STEAMAPPS_SASZA4_ID )
            if not v_apps_sasza4:
                continue
            return _Path( v["path"] ).resolve( strict=True )
    return None

"""
CSV string dictionary module.
"""
from __future__ import annotations
import typing as _typing
from io import (
    BytesIO as _BytesIO
    , StringIO as _StringIO
)
from ...util import (
    BufferLike as _BufferLike
)
from ...util.io_ import (
    SharedTextIOWrapper as _TextIOWrapper
    , is_binary_filelike as _is_binary_filelike
)
from ...util.json_ import (
    get_json_module as __get_json
)
_json = __get_json()



_ReadableFile = _typing.Union[_typing.IO[bytes], _typing.IO[str]]
_StrBytes = _typing.Union[_BufferLike, str]
LINES_SEP: str = "\r\n"
UNIT_SEP: str = "|"


class CSVItem (_typing.Tuple[str, ...]):
    """CSV string dictionary item."""


class CSV (_typing.List[CSVItem]):
    """
    CSV string dictionary.
    Inherits from `list`.

    """
    def find (
        self
        , key: str
        , default: _typing.Any = None
        , offset: int|None = None
        , multi_value: bool|None = None
    ) -> _typing.Any|list[_typing.Any]:
        """
        Get the item by given key.
        Return *default* if not found.

        Parameters
        ----------
        key: str
            Search key.
        default: Any
            Default value to be returned if key is not found.
        offset: int|None
            Search item offset.
        multi_value: bool|None
            Return multiple values of given `key`, if there's duplication.
            Default is `False`.
        """
        return _generic_find( self, key, default, offset, multi_value )


def dump (
    obj: CSV
    , fp: _ReadableFile
) -> None:
    """Serialize *obj* as a CSV-formatted stream to file-like *fp*."""
    fp = _open_as_text( fp )
    fp.writelines((
        item + LINES_SEP
        for item
        in _iter_serialize_items(obj)
    ))


def dumps (
    obj: CSV
) -> str:
    """Serialize *obj* to a CSV-formatted `str`."""
    with _StringIO( newline='' ) as fp:
        dump( obj, fp )
        return fp.getvalue()


def load (
    fp: _ReadableFile
    , maxsplit: int = -1
) -> CSV:
    """
    Deserialize file-like *fp* to a `CSV` object.

    Parameters
    ----------
    fp: file object
        File-like input object.
    maxsplit: int
        Maximum number of splits by unit separator.
        -1 means no limit.
    """
    retval = CSV()
    fp = _open_as_text( fp )
    for buf in fp:
        if buf := buf.rsplit(LINES_SEP, 1)[0]:
            retval.append( _splitline(buf, maxsplit) )
    return retval


def loads (
    inval: _StrBytes
    , maxsplit: int = -1
) -> CSV:
    """
    Deserialize *inval* to a `CSV` object.

    Parameters
    ----------
    inval: _StrBytes
        Input value.
    maxsplit: int
        Maximum number of splits by unit separator.
        -1 means no limit.
    """
    fp = None
    if isinstance( inval, str ):
        fp = _StringIO( inval, newline="" )
    else:
        fp = _BytesIO( inval )
    return load( fp, maxsplit )


def _iter_serialize_items (
    obj: CSV
) -> _typing.Iterator[str]:
    for item in obj:
        yield _toline( item )


def _splitline (
    inval: str
    , maxsplit: int = -1
) -> CSVItem:
    return CSVItem( inval.split(UNIT_SEP, maxsplit) )


def _toline (
    inval: CSVItem
) -> str:
    return UNIT_SEP.join(
        _serialize(x)
        for x
        in inval
    )


def _serialize (
    inval: _typing.Any
) -> str:
    if isinstance( inval, str ):
        return inval
    try:
        return _json.dumps( inval )
    except _json.JSONEncodeError:
        return str( inval )


def _generic_find (
    source: _typing.Iterable[_typing.Any]
    , key: _typing.Any
    , default: _typing.Any
    , offset: int|None
    , multi_value: bool|None
) -> _typing.Any|list[_typing.Any]:
    ffunc: _typing.Callable[[_typing.Sequence[_typing.Any]],bool] = (lambda x: key in x)
    if offset is not None:
        ffunc = (lambda x: x[offset] == key)
    retvals: list[_typing.Any] = []
    for item in filter( ffunc, source ):
        if multi_value:
            retvals.append( item )
        else:
            return item
    if retvals:
        return retvals
    return default


def _open_as_text (
    fp: _ReadableFile
) -> _typing.TextIO:
    if not _is_binary_filelike( fp ):
        return fp
    return _TextIOWrapper( fp, "utf8", "strict", "" )

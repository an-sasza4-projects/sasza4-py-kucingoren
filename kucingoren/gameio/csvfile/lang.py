"""
CSV language string dictionary module.
"""
from __future__ import annotations
import typing as _typing
from vdf import (
    VDFDict as _VDFDict
)
from . import (
    _generic_find as _generic_find
    , _ReadableFile as _ReadableFile
    , _StrBytes as _StrBytes
    , CSV as _CSV
    , CSVItem as _CSVItem
    , load as _load
    , loads as _loads
    , dump as _dump
    , dumps as _dumps
)



class LanguageDict (_VDFDict, _typing.Dict[str, _CSVItem]):
    """
    CSV language string dictionary.
    Inherits from `VDFDict` to allow key duplications.

    - ``value`` type is `CSVItem` object and is a tuple of an `int`, and two `str`.
    - ``key`` type is `str` and set with first element of its ``value``.

    """
    @classmethod
    def from_csv (cls, inval: _CSV) -> "LanguageDict":
        """Initiate a new instance from `CSV` object."""
        return cls( list((str(x[0]), x) for x in inval) )

    def to_csv (self) -> _CSV:
        """Serialize `self` to a `CSV` object."""
        return _CSV( self.values() )

    def find (
        self
        , key: int|str
        , default: _typing.Any = None
        , offset: int|None = None
        , multi_value: bool|None = None
    ) -> _typing.Any|list[_typing.Any]:
        """
        Get the item by given key.
        Return *default* if not found.

        Parameters
        ----------
        key: int|str
            Search key.
            If set with `int` object, it will get from `dict`'s `key` instead.
        default: Any
            Default value to be returned if key is not found.
        offset: int|None
            Search item offset.
            Key must be a `str` object.
        multi_value: bool|None
            Return multiple values of given `key`, if there's duplication.
            Default is `False`.
        """
        if isinstance( key, int ):
            if offset is not None:
                raise TypeError( f"Expected key type to be str, got %s", type(key) )
            key = str( key )
            if multi_value:
                if key not in self:
                    return default
                return self.get_all_for( key )
            return self.get( key, default )
        return _generic_find( self.values(), key, default, offset, multi_value )


def dump (
    obj: LanguageDict
    , fp: _ReadableFile
) -> None:
    """Serialize *obj* as a CSV-formatted stream to file-like *fp*."""
    return _dump( obj.to_csv(), fp )


def dumps (
    obj: LanguageDict
) -> str:
    """Serialize *obj* to a CSV-formatted `str`."""
    return _dumps( obj.to_csv() )


def load (
    fp: _ReadableFile
) -> LanguageDict:
    """Deserialize file-like *fp* to a `LanguageDict` object."""
    return LanguageDict.from_csv( _load(fp, 3) )


def loads (
    inval: _StrBytes
) -> LanguageDict:
    """Deserialize *inval* to a `LanguageDict` object."""
    return LanguageDict.from_csv( _loads(inval, 3) )

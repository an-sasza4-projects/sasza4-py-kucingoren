import pprint as pp
import io as io
from . import gameio as gio



def cprint (*args, **kwargs):
    kwargs["compact"] = True
    return pp.pprint( *args, **kwargs )


print( "Game Path:", gio.get_game_path() )
print( "Game Version:", gio.get_game_app_version() )


#main

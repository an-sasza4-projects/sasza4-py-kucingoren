"""
IO utility module.
"""
from __future__ import annotations
import typing as _typing
from io import (
    DEFAULT_BUFFER_SIZE as _BUF_SIZE
    , SEEK_END as _SEEK_END
    , TextIOWrapper as _TextIOWrapper
)
from . import (
    _T as _T
)



class SharedTextIOWrapper (_TextIOWrapper):
    """
    Same as `io.TextIOWrapper` but flush & detach upon closing.

    """
    def close (self) -> None:
        self.flush()
        self.detach()
        return super(SharedTextIOWrapper, self).close()


def is_filelike (
    class_or_instance: _typing.Any
) -> bool:
    """
    Return `True` if  `class_or_instance` has "seekable" attribute.
    Otherwise, return `False`.
    """
    return hasattr( class_or_instance, "seekable" )


def is_binary_filelike (
    class_or_instance: _typing.Any
) -> bool:
    """
    Return `True` if `class_or_instance` is file-like type
    and has "read1" attribute.
    Otherwise, return `False`.
    """
    return is_filelike( class_or_instance ) and hasattr( class_or_instance, "read1" )


def is_text_filelike (
    class_or_instance: _typing.Any
) -> bool:
    """
    Return `True` if  `class_or_instance` is file-like type
    and is not binary file-like type.
    Otherwise, return `False`.
    """
    return is_filelike( class_or_instance ) and not is_binary_filelike( class_or_instance )


def get_fileobj_size (
    fp: _typing.IO
) -> int|None:
    """
    Return size of the data start from current `tell` return value.

    File-like object must be seekable.
    Otherwise, return `None`.
    """
    data_size = None
    if fp.seekable():
        old_cursor = fp.tell()
        data_size = fp.seek( 0, _SEEK_END ) - old_cursor
        fp.seek( old_cursor )
    return data_size


def iter_fileobj_read (
    fp: _typing.IO[_T]
    , total: int|None = None
    , size: int|None = None
) -> _typing.Iterator[_T]:
    """
    Return iterator of chunks until `total` is achieved.

    Parameters
    ----------
    fp: file object
        File-like input object.
    total: int|None
        Total size of data to be achieved.
        - `None`:
            Try to guess automatically.
            Otherwise, set to -1.
        - Negatives:
            Read until EOS.

    size: int|None
        Buffer size.
    """
    if total is None:
        total = get_fileobj_size( fp )
    elif total < 0:
        total = None
    if size is None:
        size = _BUF_SIZE
    remaining = total
    read = fp.read
    while True:
        if remaining is not None:
            if remaining <= 0:
                break
            read_len = min( remaining, size )
            chunk = read( read_len )
            if not chunk:
                break
            yield chunk
            remaining -= read_len
        else:
            chunk = read( size )
            if not chunk:
                break
            yield chunk

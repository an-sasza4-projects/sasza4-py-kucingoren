"""
PKZIP 2.x archive module.
"""
from __future__ import annotations
import typing as _typing
from warnings import (
    catch_warnings as _catch_warnings
    , warn_explicit as _warn_explicit
)
import struct as _struct
from io import (
    BufferedIOBase as _BufferedIOBase
)
from shutil import (
    copyfileobj as _copyfileobj
)
from copy import (
    copy as _shallowcopy
)
from pathlib import (
    PurePath as _PPath
)
from os import (
    urandom as _urandom
)
from zipfile import (
    ZIP_LZMA as _ZIP_LZMA
    , ZIP64_LIMIT as _ZIP64_LIMIT
    , _get_compressor as _zipfile_get_compressor
    , _get_decompressor
    , crc32 as _zipfile_crc32
    , _ZipDecrypter
    , ZipInfo as _ZipInfo
    , ZipFile as _ZipFile
)
from time import (
    time as _time
    , localtime as _localtime
)
import numpy as _np
from .. import (
    copy_attr as _copyattr
)
from ..io_ import (
    iter_fileobj_read as _iread
)
from . import (
    _DateTuple as _DateTuple
    , _ReadWriteMode as _ReadWriteMode
    , _StrPath as _StrPath
    , _Compressor as _Compressor
    , _CompressorFactory as _CompressorFactory
    , _Decompressor as _Decompressor
    , _Decrypter as _Decrypter
    , _OptionalPassword as _OptionalPassword
    , _MASK_ENCRYPTED as _MASK_ENCRYPTED
    , _MASK_COMPRESS_OPTION_1 as _MASK_COMPRESS_OPTION_1
    , _MASK_USE_DATA_DESCRIPTOR as _MASK_USE_DATA_DESCRIPTOR
    , _open_read_shared as _open_read_shared
    , _read_fileinfo_header as _read_fileinfo_header
    , _get_fileheader_editor as _get_fileheader_editor
    , get_data_descriptor as _get_data_descriptor
)



# Type hints.
_Encrypter = _Decrypter
# Encryption constants.
_ENCRYPTION_HEADER_SIZE = 12
_ENCRYPTION_HEADER_KEY_SIZE = _ENCRYPTION_HEADER_SIZE-1
_crctable = _np.array( (
    0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419,
    0x706AF48F, 0xE963A535, 0x9E6495A3, 0x0EDB8832, 0x79DCB8A4,
    0xE0D5E91E, 0x97D2D988, 0x09B64C2B, 0x7EB17CBD, 0xE7B82D07,
    0x90BF1D91, 0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
    0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7, 0x136C9856,
    0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9,
    0xFA0F3D63, 0x8D080DF5, 0x3B6E20C8, 0x4C69105E, 0xD56041E4,
    0xA2677172, 0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
    0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940, 0x32D86CE3,
    0x45DF5C75, 0xDCD60DCF, 0xABD13D59, 0x26D930AC, 0x51DE003A,
    0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423, 0xCFBA9599,
    0xB8BDA50F, 0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
    0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 0x76DC4190,
    0x01DB7106, 0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F,
    0x9FBFE4A5, 0xE8B8D433, 0x7807C9A2, 0x0F00F934, 0x9609A88E,
    0xE10E9818, 0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
    0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E, 0x6C0695ED,
    0x1B01A57B, 0x8208F4C1, 0xF50FC457, 0x65B0D9C6, 0x12B7E950,
    0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3,
    0xFBD44C65, 0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
    0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB, 0x4369E96A,
    0x346ED9FC, 0xAD678846, 0xDA60B8D0, 0x44042D73, 0x33031DE5,
    0xAA0A4C5F, 0xDD0D7CC9, 0x5005713C, 0x270241AA, 0xBE0B1010,
    0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
    0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17,
    0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD, 0xEDB88320, 0x9ABFB3B6,
    0x03B6E20C, 0x74B1D29A, 0xEAD54739, 0x9DD277AF, 0x04DB2615,
    0x73DC1683, 0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
    0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1, 0xF00F9344,
    0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB,
    0x196C3671, 0x6E6B06E7, 0xFED41B76, 0x89D32BE0, 0x10DA7A5A,
    0x67DD4ACC, 0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
    0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1,
    0xA6BC5767, 0x3FB506DD, 0x48B2364B, 0xD80D2BDA, 0xAF0A1B4C,
    0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55, 0x316E8EEF,
    0x4669BE79, 0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
    0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 0xC5BA3BBE,
    0xB2BD0B28, 0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31,
    0x2CD99E8B, 0x5BDEAE1D, 0x9B64C2B0, 0xEC63F226, 0x756AA39C,
    0x026D930A, 0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
    0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38, 0x92D28E9B,
    0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21, 0x86D3D2D4, 0xF1D4E242,
    0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1,
    0x18B74777, 0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
    0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45, 0xA00AE278,
    0xD70DD2EE, 0x4E048354, 0x3903B3C2, 0xA7672661, 0xD06016F7,
    0x4969474D, 0x3E6E77DB, 0xAED16A4A, 0xD9D65ADC, 0x40DF0B66,
    0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
    0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605,
    0xCDD70693, 0x54DE5729, 0x23D967BF, 0xB3667A2E, 0xC4614AB8,
    0x5D681B02, 0x2A6F2B94, 0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B,
    0x2D02EF8D
), _np.uint32 )
"""
ZIP encryption uses the CRC32 one-byte primitive for scrambling some
internal keys. We noticed that a direct implementation is faster than
relying on binascii.crc32().
"""
"""
def _gen_crc(crc):
    for j in range(8):
        if crc & 1:
            crc = (crc >> 1) ^ 0xEDB88320
        else:
            crc >>= 1
    return crc
"""


# FIXME: Copied from Python 3.11
# Based on _ZipDecrypter
def _ZipCryptoEncrypter (
    pwd: bytes
) -> _Encrypter:
    key0 = 305419896
    key1 = 591751049
    key2 = 878082192

    global _crctable
    crctable = _crctable

    def crc32(ch, crc):
        """Compute the CRC32 primitive on one byte."""
        return (crc >> 8) ^ int(crctable[(crc ^ ch) & 0xFF])

    def update_keys(c):
        nonlocal key0, key1, key2
        key0 = crc32(c, key0)
        key1 = (key1 + (key0 & 0xFF)) & 0xFFFFFFFF
        key1 = (key1 * 134775813 + 1) & 0xFFFFFFFF
        key2 = crc32(key1 >> 24, key2)

    for p in pwd:
        update_keys(p)

    def encrypter(data):
        """Encrypt a `bytes` object."""
        result = bytearray()
        append = result.append
        for c in data:
            k = key2 | 2
            append( c ^ (((k * (k^1)) >> 8) & 0xFF) )
            update_keys(c)
        return bytes(result)

    return encrypter


class PKZipInfo (_ZipInfo):
    __slots__ = ("encryption_first_key", "orig_crc",)

    @classmethod
    def from_zipinfo (cls, inval: _ZipInfo) -> "PKZipInfo":
        return _copyattr( cls(inval.filename), inval, _shallowcopy )

    def __init__ (
        self
        , filename: str = "NoName"
        , date_time: _DateTuple = (1980,1,1,0,0,0)
        , *
        , encryption_first_key:bytes|None = None
        , orig_crc:int|None = None
    ) -> None:
        super().__init__( filename, date_time )
        self.encryption_first_key = encryption_first_key
        self.orig_crc = orig_crc


# FIXME: Merged from Python 3.11 & zipencrypt 0.3.1
# Based on zipinfo._ZipWriteFile
# and zipencrypt._ZipWriteFile
class _ZipCryptoWriteFile (_BufferedIOBase):
    __slots__ = set((
        "_zinfo"
        , "_zip64"
        , "_zipfile"
        , "_compressor"
        , "_file_size"
        , "_compress_size"
        , "_crc"
        , "_orig_crc"
        , "_encrypter"
        , "_decrypter"
        , "_decompressor"
        , "_update_infolist"
    ))

    def __init__ (
        self
        , zf: _ZipFile
        , zinfo: _ZipInfo
        , zip64: bool
        , pwd: bytes|None
        , update_infolist: bool|None = None
    ) -> None:
        self._zinfo = zinfo
        self._zip64 = zip64
        self._zipfile = zf
        compressor: _CompressorFactory = getattr( zf, "get_compressor", _zipfile_get_compressor )
        if not isinstance( compressor, _typing.Callable ):
            raise TypeError( "compressor: expected callable, got %s" % type(compressor).__name__ )
        self._compressor: _Compressor = compressor(zinfo.compress_type, zinfo._compresslevel)
        self._file_size = 0
        self._compress_size = 0
        self._crc = 0
        self._orig_crc: int|None = None
        self._encrypter: _typing.Callable[[bytes], bytes]|None = None
        self._decrypter: _typing.Callable[[bytes], bytes]|None = None
        self._decompressor: _Decompressor|None = None
        self._pwd: bytes|None = pwd
        if pwd:
            self._init_encrypter( pwd )
            self._decrypter = _ZipDecrypter( pwd )
            self._decompressor = _get_decompressor( zinfo.compress_type )
            if not zinfo.flag_bits & _MASK_USE_DATA_DESCRIPTOR:
                self._orig_crc = (
                    getattr(zinfo, "orig_crc", None)
                    or getattr(zinfo, "CRC", None)
                    or None
                )
            self._encrypt_write( get_encryption_header(zinfo, self._orig_crc) )
        if update_infolist is None:
            update_infolist = True
        self._update_infolist: bool = update_infolist

    @property
    def _fileobj (self):
        return self._zipfile.fp

    def writable (self) -> bool:
        return True

    def write (self, data) -> int:
        if self.closed:
            raise ValueError( 'I/O operation on closed file.' )

        # Accept any data that supports the buffer protocol
        if isinstance( data, (bytes, bytearray) ):
            nbytes = len( data )
        else:
            data = memoryview( data )
            nbytes = data.nbytes
        self._file_size += nbytes

        self._crc = _zipfile_crc32( data, self._crc )
        if self._compressor:
            data = self._compressor.compress( data )
            self._compress_size += len( data )
        self._encryptable_fileobj_write( data )
        return nbytes

    def close (self) -> None:
        if self.closed:
            return
        try:
            super(_ZipCryptoWriteFile, self).close()
            # Flush any data from the compressor, and update header info
            if self._compressor:
                buf: bytes = self._compressor.flush()
                self._compress_size += len( buf )
                self._encryptable_fileobj_write( buf )
                self._zinfo.compress_size = self._compress_size
            else:
                self._zinfo.compress_size = self._file_size
            self._zinfo.CRC = self._crc
            self._zinfo.file_size = self._file_size

            if self._encrypter:
                # Add encryption header size.
                self._zinfo.compress_size += _ENCRYPTION_HEADER_SIZE

            # Write updated header info
            if self._zinfo.flag_bits & _MASK_USE_DATA_DESCRIPTOR:
                # Write CRC and file sizes after the file data
                self._fileobj.write(_get_data_descriptor( self._zinfo, self._zip64 ))
                self._zipfile.start_dir = self._fileobj.tell()
            else:
                if not self._zip64:
                    if self._file_size > _ZIP64_LIMIT:
                        raise RuntimeError(
                            'File size unexpectedly exceeded ZIP64 limit')
                    if self._compress_size > _ZIP64_LIMIT:
                        raise RuntimeError(
                            'Compressed size unexpectedly exceeded ZIP64 limit')
                # Seek backwards and write file header (which will now include
                # correct CRC and file sizes)

                # Preserve current position in file
                self._zipfile.start_dir = self._fileobj.tell()
                self._fileobj.seek(self._zinfo.header_offset)
                fh_editor: _typing.Callable[[bytes],bytes] = _get_fileheader_editor( self._zipfile )
                self._fileobj.write( fh_editor(self._zinfo.FileHeader(self._zip64)) )
                if self._encrypter and self._crc != self._orig_crc:
                    # Rewrite the file data.
                    self._encrypt_rewrite()
                self._fileobj.seek(self._zipfile.start_dir)

            # Successfully written: Add file to our caches
            if self._update_infolist:
                self._zipfile.filelist.append(self._zinfo)
                self._zipfile.NameToInfo[self._zinfo.filename] = self._zinfo
        finally:
            self._zipfile._writing = False

    def _init_encrypter (self, pwd: bytes) -> None:
        self._encrypter = _ZipCryptoEncrypter( pwd )

    def _encryptable_fileobj_write (self, data: bytes) -> int:
        if self._encrypter:
            return self._encrypt_write( data )
        return self._fileobj.write( data )

    def _encrypt_write (self, data: bytes) -> int:
        return self._fileobj.write( self._encrypter(data) )

    # TODO: Pretty slow, need to revisit later.
    def _encrypt_rewrite (self) -> None:
        zinfo = self._zinfo
        # Set valid CRC value.
        self._orig_crc = zinfo.CRC
        if hasattr( zinfo, "orig_crc" ):
            setattr( zinfo, "orig_crc", self._orig_crc )
        tell = self._fileobj.tell
        seek = self._fileobj.seek
        read = self._fileobj.read
        write = self._encrypt_write
        decrypt = self._decrypter
        decompressor = self._decompressor
        decompress = (lambda x:decompressor.decompress(x, 4096)) if decompressor else None
        rewindpos = tell()
        # Reset the encrypter.
        self._init_encrypter( self._pwd )
        if decrypt:
            # Init the decrypter.
            decrypt( read(_ENCRYPTION_HEADER_SIZE) )
            seek( rewindpos )
        data_len = zinfo.compress_size - write( get_encryption_header(zinfo) )
        if data_len < 0:
            raise RuntimeError( f"{zinfo.filename}: expected positive data length, got {data_len}" )
        rewindpos = tell()
        crc_rewrt: int = 0
        for chunk in _iread( self._fileobj, data_len ):
            # Read raw data.
            if decrypt:
                chunk = decrypt( chunk )
            crc_chunk: bytes = chunk
            if decompress:
                crc_chunk = decompress( crc_chunk )
            crc_rewrt = _zipfile_crc32( crc_chunk, crc_rewrt )
            # and then rewrite it with encrypted data.
            seek( rewindpos )
            write( chunk )
            rewindpos = tell()
        if decompressor:
            crc_chunk = _zipfile_crc32( decompressor.flush(), crc_rewrt )
        if crc_rewrt != self._orig_crc:
            raise RuntimeError( f"{zinfo.filename}: expected CRC {crc_rewrt}, got {self._orig_crc}" )


class PKZipFile (_ZipFile):
    @classmethod
    def get_compressor (
        cls
        , compress_type: int
        , compresslevel: int|None = None
    ) -> _Compressor:
        """Get compressor object with given parameters."""
        return _zipfile_get_compressor( compress_type, compresslevel )

    @classmethod
    def edit_local_fileheader (cls, fileheader: bytes) -> bytes:
        """
        Edit the `zipfile.ZipInfo.FileHeader` `bytes` result
        before writing into local file header region.
        """
        return fileheader

    def on_file_overwrite (self, zinfo_new: _ZipInfo, zinfo_old: _ZipInfo) -> _ZipInfo:
        """Callback function on overwrite duplicate files. Return `zinfo_new`."""
        return to_pkzipinfo( zinfo_new, ref=zinfo_old )

    # FIXME: Copied from Python 3.11
    def write(
        self
        , filename: _StrPath
        , arcname: _StrPath|None = None
        , compress_type: int|None = None
        , compresslevel: int|None = None
        , **kwargs
    ) -> None:
        """
        Write the file named `filename` to the archive,
        giving it the archive name `arcname` (by default,
        this will be the same as `filename`,
        but without a drive letter
        and with leading path separators removed).

        If given, `compress_type` overrides the value given
        for the compression parameter to the constructor for the new entry.
        Similarly, `compresslevel` will override the constructor if given.
        The archive must be open with mode 'w', 'x' or 'a'.

        For `kwargs`, see `PKZipInfo.open`.
        """
        if not self.fp:
            raise ValueError(
                "Attempt to write to ZIP archive that was already closed")
        if self._writing:
            raise ValueError(
                "Can't write to ZIP archive while an open writing handle exists"
            )

        zinfo = _ZipInfo.from_file(filename, arcname,
                                  strict_timestamps=self._strict_timestamps)

        if zinfo.is_dir():
            zinfo.compress_size = 0
            zinfo.CRC = 0
            self.mkdir(zinfo)
        else:
            zinfo = to_pkzipinfo( zinfo )
            pwd = self._get_pwd( kwargs.get("pwd", None) )
            kwargs.setdefault( "pwd", pwd )

            if compress_type is not None:
                zinfo.compress_type = compress_type
            else:
                zinfo.compress_type = self.compression

            if compresslevel is not None:
                zinfo._compresslevel = compresslevel
            else:
                zinfo._compresslevel = self.compresslevel

            with open(filename, "rb") as src:
                # Compute the CRC32 before encrypting, if any.
                if pwd:
                    read: _typing.Callable[[], bytes] = (lambda: src.read( 2 ** 20 ))
                    crc: int = 0
                    while buf := read():
                        crc = _zipfile_crc32( buf, crc )
                    zinfo.orig_crc = crc
                    src.seek( 0 )
                with self.open(zinfo, 'w', **kwargs) as dest:
                    _copyfileobj(src, dest, 1024*8)

    # FIXME: Copied from Python 3.11
    def writestr(
        self
        , zinfo_or_arcname: _ZipInfo|str
        , data: str|bytes
        , compress_type: int|None = None
        , compresslevel: int|None = None
        , **kwargs
    ) -> None:
        """
        Write a file into the archive.
        The contents is `data`, which may be either a `str` or a `bytes` instance;
        if it is a `str`, it is encoded as UTF-8 first.
        `zinfo_or_arcname` is either the file name it will be given in the archive,
        or a `zipfile.ZipInfo` instance.
        If it's an instance, at least the filename, date, and time must be given.
        If it's a name, the date and time is set to the current date and time.
        The archive must be opened with mode 'w', 'x' or 'a'.

        If given, `compress_type` overrides the value given for the compression parameter
        to the constructor for the new entry,
        or in the `zinfo_or_arcname` (if that is a `zipfile.ZipInfo` instance).
        Similarly, `compresslevel` will override the constructor if given.

        For `kwargs`, see `PKZipInfo.open`.
        """
        if isinstance(data, str):
            data = data.encode("utf-8")
        if not isinstance(zinfo_or_arcname, _ZipInfo):
            zinfo = _ZipInfo(filename=zinfo_or_arcname,
                            date_time=_localtime(_time())[:6])
            zinfo.compress_type = self.compression
            zinfo._compresslevel = self.compresslevel
            if zinfo.filename[-1] == '/':
                zinfo.external_attr = 0o40775 << 16   # drwxrwxr-x
                zinfo.external_attr |= 0x10           # MS-DOS directory flag
            else:
                zinfo.external_attr = 0o600 << 16     # ?rw-------
        else:
            zinfo = zinfo_or_arcname

        if not self.fp:
            raise ValueError(
                "Attempt to write to ZIP archive that was already closed")
        if self._writing:
            raise ValueError(
                "Can't write to ZIP archive while an open writing handle exists."
            )

        zinfo = to_pkzipinfo( zinfo )

        if compress_type is not None:
            zinfo.compress_type = compress_type

        if compresslevel is not None:
            zinfo._compresslevel = compresslevel

        zinfo.file_size = len(data)            # Uncompressed size

        pwd = self._get_pwd( kwargs.get("pwd", None) )
        kwargs.setdefault( "pwd", pwd )
        if pwd:
            zinfo.orig_crc = _zipfile_crc32( data, 0 )

        with self._lock:
            with self.open(zinfo, mode='w', **kwargs) as dest:
                dest.write(data)

    # FIXME: Copied from Python 3.11
    def mkdir(
        self
        , zinfo_or_directory_name: _ZipInfo|str
        , mode: int|None = None
        , *
        , allow_overwrite: bool|None = None
        , allow_duplicate: bool|None = None
    ) -> None:
        """
        Create a directory inside the archive.

        The archive must be opened with mode 'w', 'x' or 'a'.

        Parameters
        ----------
        zinfo_or_directory_name: zipfile.ZipInfo|str
            If `zinfo_or_directory` is a string, a directory is created
            inside the archive with the mode that is specified
            in the `mode` argument.
            If, however, `zinfo_or_directory` is a `zipfile.ZipInfo` instance
            then the `mode` argument is ignored.
        mode: int|None
            Directory mode.
            Default is 511.
        allow_overwrite: bool|None
            Set `True` to allow overwrite existing directory.
            Default is `False`.
        allow_duplicate: bool|None
            Set `True` to allow duplication of existing directory.
            Default is `True`.
            Ignored if `allow_overwrite` is set to `True`.
        """
        if mode is None:
            mode = 511
        if isinstance(zinfo_or_directory_name, _ZipInfo):
            zinfo = zinfo_or_directory_name
            if not zinfo.is_dir():
                raise ValueError("The given ZipInfo does not describe a directory")
        elif isinstance(zinfo_or_directory_name, str):
            directory_name = zinfo_or_directory_name
            if not directory_name.endswith("/"):
                directory_name += "/"
            zinfo = _ZipInfo(directory_name)
            zinfo.compress_size = 0
            zinfo.CRC = 0
            zinfo.external_attr = ((0o40000 | mode) & 0xFFFF) << 16
            zinfo.file_size = 0
            zinfo.external_attr |= 0x10
        else:
            raise TypeError("Expected type str or ZipInfo")

        if allow_overwrite is None:
            allow_overwrite = False
        if allow_duplicate is None:
            allow_duplicate = True
        allow_duplicate = allow_duplicate and not allow_overwrite

        with self._lock:
            if self._seekable:
                self.fp.seek(self.start_dir)
            zinfo.header_offset = self.fp.tell()  # Start of header bytes
            if zinfo.compress_type == _ZIP_LZMA:
            # Compressed data includes an end-of-stream (EOS) marker
                zinfo.flag_bits |= _MASK_COMPRESS_OPTION_1

            zinfo = self._pkzip_writecheck(zinfo, allow_overwrite, allow_duplicate)
            self._didModify = True

            self.filelist.append(zinfo)
            self.NameToInfo[zinfo.filename] = zinfo
            self.fp.write( self.edit_local_fileheader(zinfo.FileHeader(False)) )
            self.start_dir = self.fp.tell()

    # FIXME: Copied from Python 3.11
    # Partially based on zipfile.ZipFile.open
    def open(
        self
        , name: str|_ZipInfo
        , mode: _ReadWriteMode|None = None
        , pwd: _OptionalPassword = None
        , *
        , compress_type: int|None = None
        , compresslevel: int|None = None
        , allow_overwrite: bool|None = None
        , allow_duplicate: bool|None = None
        , **kwargs
    ):
        """
        Return file-like object for `name`.

        Parameters
        ----------
        name: str|zipfile.ZipInfo
            String for the file name within the ZIP file, or a `zipfile.ZipInfo`
            object.
        mode: str|None
            Should be 'r' to read a file already in the ZIP file, or 'w' to
            write to a file newly added to the archive.
            Default is "r".
        pwd: bytes|False|None
            The password to decrypt or encrypt files. `False` for
            no-password writing. `None` goes fallback to default password.
        compress_type: int|None
            If given, `compress_type` overrides the value given for the compression parameter
            to the constructor for the new entry,
            or in the `name` (if that is a `zipfile.ZipInfo` instance).
        compresslevel: int|None
            If given, `compresslevel` overrides the value given for the compression parameter
            to the constructor for the new entry,
            or in the `name` (if that is a `zipfile.ZipInfo` instance).
        allow_overwrite: bool|None
            Allow to overwrite existing files, if opened with write mode.
            Default is `False`.
        allow_duplicate: bool|None
            Allow to duplicate existing files, if opened with write mode.
            Default is `True`.
            Ignored if `allow_overwrite` is set to `True`.
        **kwargs:
            See `zipfile.ZipFile.open` for available arguments.
        """
        if mode is None:
            mode = "r"
        pwd = self._get_pwd( pwd )
        if mode == "w":
            return self._pkzip_open_w(
                name
                , pwd
                , compress_type=compress_type
                , compresslevel=compresslevel
                , allow_overwrite=allow_overwrite
                , allow_duplicate=allow_duplicate
                , **kwargs
            )
        return super(PKZipFile, self).open( name, mode, pwd, **kwargs )

    def _pkzip_open_w (
        self
        , name: str|_ZipInfo
        , pwd: bytes|None = None
        , *
        , compress_type: int|None = None
        , compresslevel: int|None = None
        , allow_overwrite: bool|None = None
        , allow_duplicate: bool|None = None
        , **kwargs
    ) -> _ZipCryptoWriteFile:
        if pwd is None:
            pwd = self.pwd
        if pwd and not isinstance( pwd, bytes ):
            raise TypeError( "pwd: expected bytes, got %s" % type(pwd).__name__ )
        if not self.fp:
            raise ValueError(
                "Attempt to use ZIP archive that was already closed")
        if allow_overwrite is None:
            allow_overwrite = False
        if allow_duplicate is None:
            allow_duplicate = True
        allow_duplicate = allow_duplicate and not allow_overwrite
        # Make sure we have an info object
        if isinstance(name, _ZipInfo):
            # 'name' is already an info object
            zinfo = name
        else:
            zinfo = _ZipInfo(name)
            zinfo.compress_type = self.compression
            zinfo._compresslevel = self.compresslevel
        if compress_type is not None:
            zinfo.compress_type = compress_type
        if compresslevel is not None:
            zinfo._compresslevel = compresslevel
        # Fix different header error.
        zinfo.orig_filename = _PPath( zinfo.orig_filename ).as_posix()
        return self._pkzip_open_to_write(
            zinfo
            , **kwargs
            , pwd=pwd
            , allow_overwrite=allow_overwrite
            , allow_duplicate=allow_duplicate
        )

    # FIXME: Merged from Python 3.11 & zipencrypt 0.3.1
    # Based on zipfile.ZipFile._open_to_write
    # and zipencrypt.ZipFile._open_to_write
    def _pkzip_open_to_write (
        self
        , zinfo: _ZipInfo
        , force_zip64: bool|None = None
        , *
        , pwd: bytes|None
        , allow_overwrite: bool
        , allow_duplicate: bool
    ) -> _ZipCryptoWriteFile:
        if force_zip64 is None:
            force_zip64 = False
        if force_zip64 and not self._allowZip64:
            raise ValueError(
                "force_zip64 is True, but allowZip64 was False when opening "
                "the ZIP file."
            )
        if self._writing:
            raise ValueError("Can't write to the ZIP file while there is "
                             "another write handle open on it. "
                             "Close the first handle before opening another.")

        zinfo = to_pkzipinfo( zinfo )

        # Size and CRC are overwritten with correct data after processing the file
        zinfo.compress_size = 0
        zinfo.CRC = 0

        zinfo.flag_bits = 0x00
        if zinfo.compress_type == _ZIP_LZMA:
            # Compressed data includes an end-of-stream (EOS) marker
            zinfo.flag_bits |= _MASK_COMPRESS_OPTION_1
        if not self._seekable:
            zinfo.flag_bits |= _MASK_USE_DATA_DESCRIPTOR

        if not zinfo.external_attr:
            zinfo.external_attr = 0o600 << 16  # permissions: ?rw-------

        # Compressed size can be larger than uncompressed size
        zip64: bool = self._allowZip64 and \
                (force_zip64 or zinfo.file_size * 1.05 > _ZIP64_LIMIT)

        if self._seekable:
            self.fp.seek(self.start_dir)
        zinfo.header_offset = self.fp.tell()

        # Directory should not be encrypted.
        if zinfo.is_dir():
            pwd = None

        if pwd:
            zinfo.flag_bits |= (
                _MASK_ENCRYPTED
                #| _MASK_USE_DATA_DESCRIPTOR
            ) # set stream and encrypted
            zinfo._raw_time = (
                zinfo.date_time[3] << 11
                | zinfo.date_time[4] << 5
                | (zinfo.date_time[5] // 2))

        zinfo = self._pkzip_writecheck(zinfo, allow_overwrite, allow_duplicate)
        self._didModify = True

        self.fp.write( self.edit_local_fileheader(zinfo.FileHeader(zip64)) )

        self._writing = True
        # Overwritten files should be handled manually
        # in `on_file_overwrite` callback.
        should_update_infolist = zinfo.filename not in self.NameToInfo
        return _ZipCryptoWriteFile( self, zinfo, zip64, pwd, should_update_infolist )

    def _pkzip_writecheck (
        self
        , zinfo: _ZipInfo
        , allow_overwrite: bool
        , allow_duplicate: bool
    ) -> _ZipInfo:
        exc_warns = []
        with _catch_warnings( record=True ) as wrn:
            self._writecheck( zinfo )
            exc_warns = wrn
        for x in exc_warns:
            if (
                isinstance(x.message, UserWarning)
                and x.message.args
                and "Duplicate name:" in str(x.message.args[0])
            ):
                args = x.message.args
                msg = str(args[0])
                if allow_overwrite:
                    zinfo = self._file_overwrite( zinfo )
                    msg = msg.replace(
                        "Duplicate name:"
                        , "Overwrite duplicate name:"
                        , 1
                    )
                    x.message.args = (msg, *args[1:])
                elif not allow_duplicate:
                    raise ValueError( x.message )
            _warn_explicit(
                message=x.message
                , category = None
                , filename=x.filename
                , lineno=x.lineno
                , source=x.source
            )
        return zinfo

    def _file_overwrite (self, zinfo: _ZipInfo) -> _ZipInfo:
        if not isinstance( zinfo, _ZipInfo ):
            raise TypeError( "zinfo: expected ZipInfo, got %s" % type(zinfo).__name__ )
        name = zinfo.filename
        if name not in self.NameToInfo:
            raise ValueError( "File name %r doesn't exists" % name )
        zinfo_old = self.NameToInfo[name]
        zinfo = self.on_file_overwrite( zinfo, zinfo_old ) or zinfo
        # Replace existing zipinfo with new one
        # only if not deleted.
        self._replace_fileinfo( zinfo, zinfo_old )
        return zinfo

    def _replace_fileinfo (self, zinfo: _ZipInfo, ref: _ZipInfo|None = None) -> bool:
        name = getattr( ref, "filename", None ) or zinfo.filename
        if name not in self.NameToInfo:
            return False
        zinfo_target = self.NameToInfo[name]
        i = self.filelist.index( zinfo_target )
        self.filelist[i] = self.NameToInfo[name] = zinfo
        del zinfo_target
        return True

    def _get_pwd (self, pwd: _OptionalPassword) -> bytes|None:
        if pwd is None:
            return self.pwd
        if pwd is False:
            return b''
        return pwd


def to_pkzipinfo (
    zinfo: _ZipInfo
    , *
    , ref: _ZipInfo|None = None
    , copy_key: bool|None = None
    , copy_crc: bool|None = None
) -> PKZipInfo:
    """
    Return a new copy of `PKZipInfo`, if it's not `PKZipInfo` already.
    Otherwise, return `zinfo`.

    Parameters
    ----------
    zinfo: zipfile.ZipInfo
        Source zip info.
    ref: zipfile.ZipInfo|None
        Primary reference to `zinfo` to retrieve attributes.
    copy_key: bool|None
        Set `True` to copy `encryption_first_key` attribute.
        Default is `False`.
    copy_crc: bool|None
        Set `True` to copy `CRC` attribute.
        Default is `True`.
    """
    if copy_key is None:
        copy_key = False
    if copy_crc is None:
        copy_crc = True
    if isinstance( zinfo, PKZipInfo ):
        zinfo_new = zinfo
    else:
        zinfo_new = PKZipInfo.from_zipinfo( zinfo )
    if copy_key:
        zinfo_new.encryption_first_key = (
            getattr(ref, "encryption_first_key", None)
            or getattr(zinfo, "encryption_first_key", None)
            or getattr(zinfo_new, "encryption_first_key", None)
            or None
        )
    if copy_crc:
        zinfo_new.orig_crc = (
            getattr(ref, "orig_crc", None)
            or getattr(ref, "CRC", None)
            or getattr(zinfo, "orig_crc", None)
            or getattr(zinfo, "CRC", None)
            or getattr(zinfo_new, "orig_crc", None)
            or getattr(zinfo_new, "CRC", None)
            or None
        )
    return zinfo_new


def get_encryption_header (
    zinfo: _ZipInfo
    , crc: int|None = None
) -> bytes:
    """Return file info's encryption header `bytes`."""
    check_byte: int = 0
    if zinfo.flag_bits & _MASK_USE_DATA_DESCRIPTOR:
        check_byte = (zinfo._raw_time >> 8) & 0xff
    else:
        if crc is None:
            crc = getattr(zinfo, "orig_crc", None) or getattr(zinfo, "CRC", None) or 0
        check_byte = (crc >> 24) & 0xff
    first_key = (
        getattr(zinfo, "encryption_first_key", None)
        or _urandom(_ENCRYPTION_HEADER_KEY_SIZE)
    )
    return first_key[:_ENCRYPTION_HEADER_KEY_SIZE] + _struct.pack( "B", check_byte )


# FIXME: Copied from Python 3.11
# Based on zipinfo.ZipExtFile.__init__
# and zipinfo.ZipFile.open
def read_encryption_key (
    zfile: _ZipFile
    , zinfo: _ZipInfo
    , pwd: bytes
) -> bytes:
    """Return first 11 `bytes` of encryption header from given parameters."""
    check_byte: int = 0
    if zinfo.flag_bits & _MASK_USE_DATA_DESCRIPTOR:
        # compare against the file type from extended local headers
        check_byte = (zinfo._raw_time >> 8) & 0xff
    else:
        # compare against the CRC otherwise
        check_byte = (zinfo.CRC >> 24) & 0xff
    decrypter: _Decrypter = _ZipDecrypter( pwd )
    # The first 12 bytes in the cypher stream is an encryption header
    #  used to strengthen the algorithm. The first 11 bytes are
    #  completely random, while the 12th contains the MSB of the CRC,
    #  or the MSB of the file time depending on the header type
    #  and is used to check the correctness of the password.
    zef_file: _typing.IO[bytes] = _open_read_shared( zfile, zinfo.header_offset )
    try:
        rewindpos = zef_file.tell()
        # Skip the file header:
        _read_fileinfo_header( zef_file )

        header = decrypter( zef_file.read(_ENCRYPTION_HEADER_SIZE) )
        zef_file.seek( rewindpos )
    finally:
        zef_file.close()
    h = header[-1]
    if h != check_byte:
        raise RuntimeError("Bad password for file %r" % zinfo.orig_filename)
    return header[:_ENCRYPTION_HEADER_KEY_SIZE]

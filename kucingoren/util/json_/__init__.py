"""
JSON utility module.
"""
from functools import (
    lru_cache as _cache
)



@_cache( 1 )
def get_json_module ():
    """
    Return either `orjson` or `json` module based on currently available module.

    The return value is memoized.
    """
    try:
        from . import _orjson
        return _orjson
    except ImportError:
        from . import _json
        return _json

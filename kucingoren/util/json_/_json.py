"""
Built-in JSON module loader.
"""
from __future__ import annotations
import typing as _typing
import json.encoder as _json_enc
from dataclasses import (
    is_dataclass as _is_dataclass
    , asdict as _asdict
)
from datetime import (
    datetime as _datetime
    , date as _date
    , time as _time
)
from uuid import (
    UUID as _UUID
)
from enum import (
    Enum as _Enum
)
from numpy import (
    ndarray as _np_ndarray
    , generic as _np_generic
    , bool_ as _np_bool
    , integer as _np_integer
    , inexact as _np_inexact
)
from ...util.io_ import (
    SharedTextIOWrapper as _SharedTextIOWrapper
    , is_binary_filelike as _is_binary_filelike
)
from json import *
from json import (
    __all__ as _json_all__
    , _default_encoder as _orig_default_encoder
    , dump as _orig_dump
    , dumps as _orig_dumps
)
__all__ = _json_all__ + [
    "JSONEncodeError"
    , "ORJSONEncoder"
]



_MISSING = object()


class JSONEncodeError (ValueError):
    """
    Subtitution of `orjson.JSONEncodeError`.
    It inherits from `ValueError`.

    """


class ORJSONEncoder (JSONEncoder):
    """
    Basic `orjson`-alike JSON encoder.

    """
    def __init__(
        self
        , *
        , skipkeys: bool = False
        , ensure_ascii: bool = True
        , check_circular: bool = True
        , allow_nan: bool = True
        , sort_keys: bool = False
        , indent: int|None = None
        , separators: tuple[str, str]|None = None
        , default: _typing.Callable[..., _typing.Any]|None = None
    ) -> None:
        # Replicating `orjson`'s `default` behaviour.
        # First, it goes to class `default` method,
        # if it raise `TypeError`,
        # then goes to user-defined `default` function.
        if default:
            def _wrapper (obj, *, cls=type(self), self=self, default=default):
                try:
                    return cls.default( self, obj )
                except TypeError:
                    return default( obj )
            default = _wrapper
        super().__init__(
            skipkeys=skipkeys, ensure_ascii=ensure_ascii, check_circular=check_circular
            , allow_nan=allow_nan, sort_keys=sort_keys, indent=indent
            , separators=separators, default=default
        )

    def default (self, obj):
        retval = _MISSING
        # dataclass.
        if _is_dataclass( obj ):
            retval = _asdict( obj )
        # datetime.
        if isinstance( obj, (_datetime,_date,_time,) ):
            retval = obj.isoformat()
        # enum.
        if isinstance( obj, _Enum ):
            retval = obj.value
        # numpy.
        if isinstance( obj, _np_ndarray ):
            retval = obj.tolist()
        elif isinstance( obj, _np_generic ):
            if isinstance( obj, _np_bool ):
                retval = bool( obj )
            elif isinstance( obj, _np_integer ):
                retval = int( obj )
            elif isinstance( obj, _np_inexact ):
                retval = float( obj )
            else:
                retval = str( obj )
        # uuid.
        if isinstance( obj, _UUID ):
            retval = str( obj )
        if retval is not _MISSING:
            return retval
        # default.
        return super(ORJSONEncoder, self).default( obj )

    # FIXME: Copied from Python 3.11
    def iterencode (
        self
        , o: _typing.Any
        , _one_shot: bool = False
    ) -> _typing.Iterator[str]:
        if self.check_circular:
            markers: dict[int, _typing.Any]|None = {}
        else:
            markers = None
        if self.ensure_ascii:
            _encoder = _json_enc.encode_basestring_ascii
        else:
            _encoder = _json_enc.encode_basestring

        def floatstr(
            o: float
            , allow_nan: bool = self.allow_nan
            , _repr: _typing.Callable[[_typing.Any],str] = float.__repr__
            , _inf: float = _json_enc.INFINITY
            , _neginf: float = -_json_enc.INFINITY
        ) -> str:
            # Check for specials.  Note that this type of test is processor
            # and/or platform-specific, so do tests which don't depend on the
            # internals.
            if o != o:
                text = 'NaN'
            elif o == _inf:
                text = 'Infinity'
            elif o == _neginf:
                text = '-Infinity'
            elif getattr( o, "__repr__", None ):
                return repr( o )
            else:
                return _repr(o)

            if not allow_nan:
                raise ValueError(
                    "Out of range float values are not JSON compliant: " +
                    repr(o))

            return text

        def intstr (
            o: int
            , _repr: _typing.Callable[[_typing.Any],str] = int.__repr__
        ) -> str:
            if getattr( o, "__repr__", None ):
                return repr( o )
            return _repr( o )

        _iterencode = _make_iterencode(
            markers, self.default, _encoder, self.indent, floatstr,
            self.key_separator, self.item_separator, self.sort_keys,
            self.skipkeys, _one_shot
            , _intstr=intstr
        )
        return _iterencode( o, 0 )


_default_encoder: JSONEncoder = ORJSONEncoder(
    skipkeys = False,
    ensure_ascii = True,
    check_circular = True,
    allow_nan = True,
    indent = None,
    separators = None,
    default = None,
)


def _is_default_encoder (
    skipkeys: bool
    , ensure_ascii: bool
    , check_circular: bool
    , allow_nan: bool
    , cls: _typing.Type[JSONEncoder]|None
    , indent: int|None
    , separators: tuple[str,str]|None
    , default: _typing.Callable[[_typing.Any],_typing.Any]|None
    , sort_keys: bool
    , **kw
) -> bool:
    return (
        not skipkeys and ensure_ascii and
        check_circular and allow_nan and
        cls is None and indent is None and separators is None and
        default is None and not sort_keys and not kw
    )


def _make_iterencode (
    *args
    , **kwargs
) -> _typing.Callable[[_typing.Any, int], _typing.Iterator[str]]:
    constructor = _json_enc._make_iterencode( *args, **kwargs )
    def factory (*args, **kwargs) -> _typing.Iterator[str]:
        try:
            yield from constructor( *args, **kwargs )
        except BaseException as exc:
            # Shallow all errors and be one!
            raise JSONEncodeError( exc )
    return factory


# FIXME: Copied from Python 3.11
def dumps (
    obj
    , *
    , skipkeys = False
    , ensure_ascii = True
    , check_circular = True
    , allow_nan = True
    , cls = None
    , indent = None
    , separators = None
    , default = None
    , sort_keys = False
    , **kw
) -> _typing.Any:
    """
    Serialize ``obj`` to a JSON formatted ``str``.

    If ``skipkeys`` is true then ``dict`` keys that are not basic types
    (``str``, ``int``, ``float``, ``bool``, ``None``) will be skipped
    instead of raising a ``TypeError``.

    If ``ensure_ascii`` is false, then the return value can contain non-ASCII
    characters if they appear in strings contained in ``obj``. Otherwise, all
    such characters are escaped in JSON strings.

    If ``check_circular`` is false, then the circular reference check
    for container types will be skipped and a circular reference will
    result in an ``OverflowError`` (or worse).

    If ``allow_nan`` is false, then it will be a ``ValueError`` to
    serialize out of range ``float`` values (``nan``, ``inf``, ``-inf``) in
    strict compliance of the JSON specification, instead of using the
    JavaScript equivalents (``NaN``, ``Infinity``, ``-Infinity``).

    If ``indent`` is a non-negative integer, then JSON array elements and
    object members will be pretty-printed with that indent level. An indent
    level of 0 will only insert newlines. ``None`` is the most compact
    representation.

    If specified, ``separators`` should be an ``(item_separator, key_separator)``
    tuple.  The default is ``(', ', ': ')`` if *indent* is ``None`` and
    ``(',', ': ')`` otherwise.  To get the most compact JSON representation,
    you should specify ``(',', ':')`` to eliminate whitespace.

    ``default(obj)`` is a function that should return a serializable version
    of obj or raise TypeError. The default simply raises TypeError.

    If *sort_keys* is true (default: ``False``), then the output of
    dictionaries will be sorted by key.

    To use a custom ``JSONEncoder`` subclass (e.g. one that overrides the
    ``.default()`` method to serialize additional types), specify it with
    the ``cls`` kwarg; otherwise ``ORJSONEncoder`` is used.
    """
    # cached encoder
    if _is_default_encoder(
        skipkeys=skipkeys, ensure_ascii=ensure_ascii, check_circular=check_circular,
        allow_nan=allow_nan, cls=cls, indent=indent, separators=separators,
        default=default, sort_keys=sort_keys, **kw
    ):
        return _default_encoder.encode(obj)
    if cls is None:
        cls = ORJSONEncoder
    return cls(
        skipkeys=skipkeys, ensure_ascii=ensure_ascii,
        check_circular=check_circular, allow_nan=allow_nan, indent=indent,
        separators=separators, default=default, sort_keys=sort_keys,
        **kw).encode(obj)


# FIXME: Copied from Python 3.11
def dump (
    obj
    , fp
    , *
    , skipkeys = False
    , ensure_ascii = True
    , check_circular = True
    , allow_nan = True
    , cls = None
    , indent = None
    , separators = None
    , default = None
    , sort_keys = False
    , **kw
):
    """
    Serialize ``obj`` as a JSON formatted stream to ``fp`` (a
    ``.write()``-supporting file-like object).

    If ``skipkeys`` is true then ``dict`` keys that are not basic types
    (``str``, ``int``, ``float``, ``bool``, ``None``) will be skipped
    instead of raising a ``TypeError``.

    If ``ensure_ascii`` is false, then the strings written to ``fp`` can
    contain non-ASCII characters if they appear in strings contained in
    ``obj``. Otherwise, all such characters are escaped in JSON strings.

    If ``check_circular`` is false, then the circular reference check
    for container types will be skipped and a circular reference will
    result in an ``OverflowError`` (or worse).

    If ``allow_nan`` is false, then it will be a ``ValueError`` to
    serialize out of range ``float`` values (``nan``, ``inf``, ``-inf``)
    in strict compliance of the JSON specification, instead of using the
    JavaScript equivalents (``NaN``, ``Infinity``, ``-Infinity``).

    If ``indent`` is a non-negative integer, then JSON array elements and
    object members will be pretty-printed with that indent level. An indent
    level of 0 will only insert newlines. ``None`` is the most compact
    representation.

    If specified, ``separators`` should be an ``(item_separator, key_separator)``
    tuple.  The default is ``(', ', ': ')`` if *indent* is ``None`` and
    ``(',', ': ')`` otherwise.  To get the most compact JSON representation,
    you should specify ``(',', ':')`` to eliminate whitespace.

    ``default(obj)`` is a function that should return a serializable version
    of obj or raise TypeError. The default simply raises TypeError.

    If *sort_keys* is true (default: ``False``), then the output of
    dictionaries will be sorted by key.

    To use a custom ``JSONEncoder`` subclass (e.g. one that overrides the
    ``.default()`` method to serialize additional types), specify it with
    the ``cls`` kwarg; otherwise ``ORJSONEncoder`` is used.
    """
    # cached encoder
    if _is_default_encoder(
        skipkeys=skipkeys, ensure_ascii=ensure_ascii, check_circular=check_circular,
        allow_nan=allow_nan, cls=cls, indent=indent, separators=separators,
        default=default, sort_keys=sort_keys, **kw
    ):
        iterable = _default_encoder.iterencode(obj)
    else:
        if cls is None:
            cls = ORJSONEncoder
        iterable = cls(skipkeys=skipkeys, ensure_ascii=ensure_ascii,
            check_circular=check_circular, allow_nan=allow_nan, indent=indent,
            separators=separators,
            default=default, sort_keys=sort_keys, **kw).iterencode(obj)
    if _is_binary_filelike( fp ):
        fp = _SharedTextIOWrapper( fp, encoding="utf8" )
    # could accelerate with writelines in some versions of Python, at
    # a debuggability cost
    for chunk in iterable:
        fp.write(chunk)

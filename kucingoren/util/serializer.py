"""
Common serializer utility module.
"""
from __future__ import annotations
from decimal import (
    Decimal as _Decimal
)
import numpy as _np
from . import (
    _T as _T
)



class LeastTwoPrecisionDigitsFloat (float):
    """
    A generic `float` object
    but when converted to `str`,
    it will include at least two precision digits.

    """
    def __repr__ (self) -> str:
        return _np.format_float_positional( self, min_digits=2 )


def serialize_f (
    inval: _T
) -> float|_T:
    """
    Serialize `float`-like `inval` to a `float`.
    Otherwise, return `inval`.
    """
    if not isinstance( inval, (float,_Decimal) ):
        return inval
    return float( inval )


def serialize_f2p (
    inval: _T
) -> LeastTwoPrecisionDigitsFloat|_T:
    """
    Serialize `float`-like to a `LeastTwoPrecisionDigitFloat`.
    Otherwise, return `inval`.
    """
    if not isinstance( inval, (float,_Decimal) ):
        return inval
    return LeastTwoPrecisionDigitsFloat( inval )

"""
Numpy dataclasses utility module.
"""
from __future__ import annotations
import typing as _typing
from dataclasses import (
    Field as _Field
    , dataclass as _create_dataclass
    , is_dataclass as _is_dataclass
    , fields as _fields
)
from functools import (
    cached_property as _cached_property
    , partial as _partial
    , lru_cache as _cache
)
import typing_extensions as _typingext
from numpy import (
    dtype as _np_dtype
    , generic as _np_dtype_generic
    , ndarray as _np_ndarray
    , asarray as _asarray
    , array as _array
    , frombuffer as _frombuffer
    , fromiter as _fromiter
    , array_equal as _array_equal
    , frompyfunc as _frompyfunc
)
from numpy._typing import (
    _SupportsArrayFunc
    , _DTypeLike
    , NDArray as _NDArray
)
from numpy._typing._dtype_like import (
    _SCT
)
from .typing_ import (
    ByteOrder as _ByteOrder
    , ShapeLike as _ShapeLike
    , DEFAULT_DTYPE as _DefaultDType
    , _NATIVE_BYTEORDER as _NATIVE_BYTEORDER
    , _SCALAR_LIKE_TYPES as _SCALAR_LIKE_TYPES
    , get_ndarray_args as _get_ndarray_args
)
from . import (
    resolve_shape as _resolve_shape
    , shape_size as _shape_size
    , resolve_base as _resolve_base
)
from .. import (
    _T as _T
    , BufferLike as _BufferLike
    , get_shapedarray_args as _get_shapedarray_args
    , is_subclass_or_instance as _is_subclass_or_instance
    , resolve_generic_origin as _resolve_generic_origin
)
from ..io_ import (
    is_binary_filelike as _is_binary_filelike
)



_T_ABCNumpyDataclass_co = _typing.TypeVar( "_T_ABCNumpyDataclass_co", bound="ABCNumpyDataclass", covariant=True )
_StructuredDType = _typing.Tuple[
    _DTypeLike,
    _typing.Union[_ShapeLike, None],
    _typing.Union[_ByteOrder, None],
]
_MISSING = object()


class BadDynamicArrayShape (TypeError):
    """Failed to resolve the shape."""
    def __init__ (self, msg: _typing.Any, field_name: str, *args) -> None:
        super().__init__( msg, field_name, *args )


class BadDType (ValueError):
    """Failed to resolve the dtype."""


@_typingext.dataclass_transform( eq_default=True )
class ABCNumpyDataclass ():
    """
    Abstract class for any dataclasses that
    fields are purely made of `numpy`-alike objects.

    """
    _default_byteorder: _typing.ClassVar[_ByteOrder|None] = None

    @classmethod
    def frombuffer (
        cls: _typing.Type[_T_ABCNumpyDataclass_co]
        , buffer: _BufferLike
        , **kwargs
    ) -> _T_ABCNumpyDataclass_co:
        """
        Return a new instance from given buffer-like object.

        Parameters
        ----------
        buffer: util.BufferLike
            A buffer-like object.
        **kwargs
            See `as_struct_dtype` for available arguments.
        """
        if "byteorder" not in kwargs:
            kwargs["byteorder"] = cls._default_byteorder
        return _cls_from( cls, _frombuffer(buffer, "B"), struct_dtype_kwargs=kwargs )

    @classmethod
    def fromfile (
        cls: _typing.Type[_T_ABCNumpyDataclass_co]
        , fp: _typing.BinaryIO
        , **kwargs
    ) -> _T_ABCNumpyDataclass_co:
        """
        Return a new instance from given binary-file-like object.

        Parameters
        ----------
        fp: typing.BinaryIO
            A binary-file-like object.
        **kwargs
            See `as_struct_dtype` for available arguments.
        """
        if "byteorder" not in kwargs:
            kwargs["byteorder"] = cls._default_byteorder
        return _cls_from( cls, fp, struct_dtype_kwargs=kwargs )

    @property
    def dtype (self) -> _np_dtype:
        """`numpy.dtype` of this struct."""
        return _dc_as_dtype( self, byteorder=self._default_byteorder )

    @property
    def nbytes (self) -> int:
        """Size of this struct in bytes."""
        return self.dtype.itemsize

    def tobytes (self, byteorder: _ByteOrder|None = _MISSING) -> bytes:
        """
        Return as `bytes` instance.

        Parameters
        ----------
        byteorder: util.numpy_.typing_.ByteOrder|None
            Change the byte order with given value.
            Set to `None` to keep it as-is.
        """
        if byteorder is _MISSING:
            byteorder = self._default_byteorder
        return self.asarray(byteorder=byteorder).tobytes()

    def tofile (self, fp: _typing.BinaryIO, byteorder: _ByteOrder|None = _MISSING):
        """
        Write array to a binary-file-like object.

        Parameters
        ----------
        fp: typing.BinaryIO
            A binary-file-like object.
        byteorder: util.numpy_.typing_.ByteOrder|None
            Change the byte order with given value.
            Set to `None` to keep it as-is.
        """
        if byteorder is _MISSING:
            byteorder = self._default_byteorder
        fp.write( self.asarray(byteorder=byteorder) )
        fp.flush()

    def asarray (self, byteorder: _ByteOrder|None = _MISSING, **kwargs) -> _np_ndarray:
        """
        Return as `numpy.ndarray` instance.

        Parameters
        ----------
        byteorder: util.numpy_.typing_.ByteOrder|None
            Change the byte order with given value.
            Set to `None` to keep it as-is.
        **kwargs:
            See `numpy.asarray` for available arguments.
        """
        if byteorder is _MISSING:
            byteorder = self._default_byteorder
        return _dc_as_array( self, byteorder=byteorder, asarray_kwargs=kwargs )

    def __bytes__ (self):
        return self.tobytes()

    def __eq__ (self, other: object) -> bool:
        if not isinstance( other, ABCNumpyDataclass ):
            return NotImplemented
        return _array_equal( self.asarray(), other.asarray() )


@_typingext.dataclass_transform( eq_default=True )
def numpy_dataclass (
    cls: _typing.Type[_T_ABCNumpyDataclass_co]|None = None
    , **kwargs
) -> _typing.Type[_T_ABCNumpyDataclass_co]|_typing.Callable[[_typing.Type[_T_ABCNumpyDataclass_co]], _typing.Type[_T_ABCNumpyDataclass_co]]:
    """
    Similar to `dataclasses.dataclass` decorator but inherit from `ABCNumpyDataclass`.

    Parameters
    ----------
    cls: typing.Type[typing.Any]
        The class.
    **kwargs
        See `dataclasses.dataclass` for available arguments.
    """
    def decorator (
        cls: _typing.Type[_T_ABCNumpyDataclass_co]
    ) -> _typing.Type[_T_ABCNumpyDataclass_co]:
        # Use our own __eq__.
        dc_kwargs = kwargs.copy()
        dc_kwargs["eq"] = False
        cls = _create_dataclass( cls, **dc_kwargs )
        if not issubclass( cls, ABCNumpyDataclass ):
            cls = type( cls.__name__, (cls, ABCNumpyDataclass,), {}, )
        # Better to cache the dtype, if it's a basic dataclass.
        if (
            isinstance(cls.dtype, property)
            # TODO: Allow cached dtype for __slots__ classes.
            and not hasattr( cls, "__slots__" )
            and not _has_dynamic_array_field(cls)
        ):
            cls.dtype = _cached_property( cls.dtype.fget )
            cls.dtype.__set_name__( cls, "dtype" )
        return cls
    # See if we're being called as @dataclass or @dataclass().
    if cls is None:
        # We're called with parens.
        return decorator
    # We're called as @dataclass without parens.
    return decorator( cls )


def is_numpy_dataclass (
    class_or_instance: _typing.Any
) -> bool:
    """
    Return `True` if `class_or_instance` is a `numpy`-alike dataclass or an instance of one.
    Otherwise, return `False`.
    """
    return (
        _is_dataclass( class_or_instance )
        and _is_subclass_or_instance( class_or_instance, ABCNumpyDataclass )
    )


def iter_struct_fields (
    class_or_instance: _typing.Any
    , **kwargs
) -> _typing.Iterable[_typing.Tuple[_Field,_StructuredDType]]:
    """
    Return iterator of (`dataclasses.Field`, `numpy.dtype`) compiled from `numpy`-alike dataclass fields.

    Parameters
    ----------
    class_or_instance: typing.Any
        Any dataclass classes or instances.
    **kwargs:
        See `as_struct_dtype` for available keyword arguments.
    """
    if isinstance( class_or_instance, type ):
        cls = class_or_instance
        instance = None
    else:
        cls = type( class_or_instance )
        instance = class_or_instance
    for field in numpy_fields( cls ):
        yield (field, as_struct_dtype(field, cls, ref=instance, **kwargs))


def _has_dynamic_array_field (
    class_or_instance: _typing.Any
) -> bool:
    """
    Return `True` if `class_or_instance` has at least one dynamic array field.
    Otherwise, return `False`.
    """
    if not isinstance( class_or_instance, type ):
        class_or_instance = type( class_or_instance )
    return _c_has_dynamic_array_field( class_or_instance )


@_cache
def _c_has_dynamic_array_field (
    cls: _typing.Type
) -> bool:
    for _,struct_dtype in iter_struct_fields(
        cls
        , resolve_dynamic_shape = False
        , cast_numpy_dataclass = False
    ):
        sd_dtype, sd_shape, _ = struct_dtype
        if _is_dynamic_shape( sd_shape, cls ):
            return True
        if is_numpy_dataclass( sd_dtype ) and _has_dynamic_array_field( sd_dtype ):
            return True
    return False


def _dc_as_array (
    obj: ABCNumpyDataclass
    , byteorder: _ByteOrder|None = None
    , *
    , asarray_kwargs: dict|None = None
):
    if asarray_kwargs is None:
        asarray_kwargs = {}
    if byteorder:
        dt = _dc_as_dtype( obj, byteorder=byteorder )
    else:
        dt = obj.dtype

    def __iter_field ():
        for name, prop in dt.fields.items():
            field_value = getattr( obj, name )
            field_dtype, *_ = prop
            # Numpy dataclasses.
            if is_numpy_dataclass( field_value ):
                yield _dc_as_array( field_value, byteorder=byteorder, asarray_kwargs=asarray_kwargs )
            # Array of a Numpy dataclass.
            elif (
                (
                    isinstance(field_value, _typing.Sequence)
                    or (isinstance(field_value, _np_ndarray) and field_value.ndim > 0)
                )
                and len(arr := _asarray(field_value, dtype=object))
                and is_numpy_dataclass(arr_first := arr.flat[0])
            ):
                is_dynamic = _has_dynamic_array_field( arr_first )
                ufunc = _frompyfunc(
                    _partial(_dc_as_array, byteorder=byteorder, asarray_kwargs=asarray_kwargs)
                    , 1, 1
                )
                # Convert all dataclass objects into their ndarray form.
                result: _np_ndarray = ufunc( arr )
                # HACK: Rebuild from scratch, no more monkey business.
                if is_dynamic:
                    result = _array( [x.view("B") for x in result.flat], "B", copy=False )
                    result = result.reshape( -1 )
                    result = result.view( field_dtype )
                yield result
            # Any `numpy.dtype`-alikes.
            else:
                yield field_value

    return _asarray(
        [tuple(__iter_field())]
        , dtype=dt
        , **asarray_kwargs
    )


def _dc_as_dtype (
    x: ABCNumpyDataclass
    , **struct_dtype_kwargs
) -> _np_dtype:
    return _np_dtype(list(
        (field.name, sd[0],)
        for field, sd
        in iter_struct_fields(x, **struct_dtype_kwargs)
    ))


def is_numpy_alike (
    field: _Field
) -> bool:
    """
    Return `True` if given `field` is `numpy`-alike dataclass field.
    Otherwise, return `False`.
    """
    field_type = field.type
    return bool(
        field.init
        and (
            field_type is None
            or _is_subclass_or_instance(field_type, _np_dtype)
            or _is_subclass_or_instance(field_type, _SCALAR_LIKE_TYPES)
            or is_numpy_dataclass(field_type)
            or _get_ndarray_args(field_type)
        )
    )


def numpy_fields (
    class_or_instance: _typing.Any
) -> tuple[_Field, ...]:
    """
    Similar to `util.dataclasses_.fields` but only return `numpy`-alike fields.
    """
    if not isinstance( class_or_instance, type ):
        class_or_instance = type( class_or_instance )
    return _c_numpy_fields( class_or_instance )


@_cache
def _c_numpy_fields (
    cls: _typing.Type
) -> tuple[_Field, ...]:
    return tuple( x for x in _fields(cls) if is_numpy_alike(x) )


def as_struct_dtype (
    field: _Field
    , cls: _typing.Type[ABCNumpyDataclass]
    , *
    , byteorder: _ByteOrder|None = None
    , ignore_non_native: bool|None = None
    , resolve_dynamic_shape: bool|None = None
    , cast_numpy_dataclass: bool|None = None
    , ref: _typing.Any|None = None
) -> _StructuredDType:
    """
    Cast `field` as a structured `numpy.dtype`.

    Parameters
    ----------
    field: dataclasses.Field
        A dataclass field.
    cls: typing.Type[ABCNumpyDataclass]
        The dataclass type which has this field.
    byteorder: util.numpy_.typing_.ByteOrder|None
        Change the byte order with given value.
        Set to `None` to keep it as-is.
    ignore_non_native: bool|None
        If `byteorder` is set, this will determine that
        any `numpy.dtype` with non-native byte order
        will be keep it as-is (ie. no byte order is changed).
        Default is `True`.
    resolve_dynamic_shape: bool|None
        Resolve the shape of dynamic arrays.
        Set to `False` to keep it as `str` object.
        Default is `True`.
    cast_numpy_dataclass: bool|None
        Cast numpy dataclasses to its own `numpy.dtype`.
        Set to `False` to keep it as `numpy.dtype` of `numpy.object_`.
        Default is `True`.
    ref: typing.Any
        The lookup reference.
        Required if `resolve_dynamic_shape` is `True`.

    Returns
    ----------
    `numpy.dtype`:
        A valid `numpy.dtype`.
    `tuple`:
        An unresolved dtype, must manually resolve & cast it into a `numpy.dtype`.
    """
    if cls is None:
        cls = type( ref )
    if ignore_non_native is None:
        ignore_non_native = True
    if resolve_dynamic_shape is None:
        resolve_dynamic_shape = True
    if cast_numpy_dataclass is None:
        cast_numpy_dataclass = True
    __dc_as_dtype = _partial(
        _dc_as_dtype
        , byteorder=byteorder
        , ignore_non_native=ignore_non_native
        , resolve_dynamic_shape=resolve_dynamic_shape
        , cast_numpy_dataclass=cast_numpy_dataclass
    )
    field_name: str = field.name
    field_dtype = field.type
    field_value = _MISSING
    field_shape: _ShapeLike|None = None
    field_byteorder = byteorder
    # Get from reference, if any.
    if ref:
        try:
            field_value = _getref( ref, field_name )
        except (AttributeError, KeyError,):
            pass
    is_dc = is_numpy_dataclass( field_dtype )
    allow_cast_dc = (lambda: bool(cast_numpy_dataclass and is_dc))
    # NDArray generics & sized arrays.
    if t_nd_args := _get_ndarray_args( field_dtype ):
        t_shape, t_dtype = t_nd_args
        is_dc = is_numpy_dataclass( t_dtype )
        is_dynamic = _is_dynamic_shape( t_shape, cls )
        # From ref.
        if field_value is not _MISSING:
            t_value = _asarray( field_value )
            t_value_dtype, t_shape = t_value.dtype, t_value.shape
            if resolve_dynamic_shape and is_dynamic:
                t_shape = t_value.shape
            # Special case for numpy dataclasses.
            if allow_cast_dc():
                # An array.
                if t_value.ndim > 0:
                    # Not empty.
                    if t_value.size:
                        # Basic dataclasses dtype is always uniform,
                        # only need to grab it once and apply the shape directly.
                        if not _has_dynamic_array_field( t_dtype ):
                            t_value_dtype = __dc_as_dtype( t_dtype )
                        # Unpack every single of them into their own dtype,
                        # and discard the shape.
                        else:
                            t_shape = None
                            def __iter_deep_cast (x, base=None):
                                if base is None:
                                    base = x
                                if isinstance(x, _np_ndarray):
                                    dtx = _np_dtype([
                                        yy
                                        for y in x
                                        for yy in __iter_deep_cast(y, base)
                                    ])
                                else:
                                    dtx = __dc_as_dtype( x )
                                if x is base:
                                    yield from dtx.descr
                                else:
                                    yield ("", dtx,)
                            t_value_dtype = list( __iter_deep_cast(t_value) )
                    # Failed to cast it into its struct dtype, return as-is.
                # Single value.
                else:
                    t_value_dtype = __dc_as_dtype( field_value )
            # Otherwise, a basic dtype.
            else:
                t_value_dtype = t_value_dtype.newbyteorder( _np_dtype(t_dtype).byteorder )
            field_shape, t_dtype = t_shape, t_value_dtype
        # From _cls.
        else:
            field_shape = t_shape
            if allow_cast_dc():
                t_dtype = __dc_as_dtype( t_dtype )
        if (
            (is_dynamic and not resolve_dynamic_shape)
            or (is_dc and not cast_numpy_dataclass)
        ):
            field_dtype = t_dtype
        elif field_shape is not None:
            field_dtype = _np_dtype( (t_dtype, field_shape) )
        else:
            field_dtype = _np_dtype( t_dtype )
    # Numpy dataclasses.
    elif is_dc:
        if allow_cast_dc():
            # From ref.
            if field_value is not _MISSING:
                field_dtype = __dc_as_dtype( field_value )
            # From _cls.
            else:
                field_dtype = __dc_as_dtype( field_dtype )
        # Not allowed to cast Numpy dataclasses, return as-is.
    # Any `numpy.dtype`-alikes.
    else:
        field_dtype = _np_dtype( field_dtype )
    if isinstance( field_dtype, _np_dtype ):
        if field_shape is None:
            field_shape = _resolve_shape( field_dtype )
        if field_byteorder:
            field_dtype = _dtype_newbyteorder( field_dtype, field_byteorder, ignore_non_native )
    return (field_dtype, field_shape, field_byteorder,)


@_cache
def _is_dynamic_shape (
    shape: _ShapeLike
    , cls: _typing.Type
) -> bool:
    return (
        isinstance( shape, str )
        and shape in (x.name for x in _fields(cls))
    )


def _resolve_dynamic_shape (
    shape: str
    , field_name: str
    , *
    , cls: _typing.Type[_typing.Any]|None = None
    , ref: _typing.Any|None = None
):
    if cls is None:
        cls = type( ref )
    if not _is_dynamic_shape( shape, cls ):
        return shape
    if ref is None:
        raise BadDynamicArrayShape(
            f"ref: expected an object, got {type(ref)}"
            , field_name
        )
    try:
        return _getref( ref, shape )
    except (AttributeError, KeyError,) as exc:
        raise BadDynamicArrayShape(
            exc
            , field_name
        )


def _parse_field (
    buffer: _BufferLike
    , field: _Field
    , cls: _typing.Type[_T_ABCNumpyDataclass_co]
    , struct_dtype: _StructuredDType
    , *
    , ref: dict[str,_typing.Any]|None = None
    , struct_dtype_kwargs: dict[str,_typing.Any]|None = None
) -> tuple[_BufferLike, str, _typing.Any]:
    if ref is None:
        ref = {}
    if struct_dtype_kwargs is None:
        struct_dtype_kwargs = {}
    ignore_non_native = struct_dtype_kwargs.get( "ignore_non_native", True )
    if isinstance( buffer, _BufferLike.__args__ ):
        __read = _readbuffer
        __read_dataclass = _readbuffer_dataclass
    elif _is_binary_filelike( buffer ):
        __read = _readfile
        __read_dataclass = _readfile_dataclass
    else:
        raise TypeError(
            "buffer: expected buffer-like or binary file-like, got %s"
            % type(buffer).__name__
        )
    f_name, f_type = field.name, _resolve_generic_origin( field.type )
    sd_dtype, sd_shape, byteorder = struct_dtype
    is_dtype_dc = is_numpy_dataclass( sd_dtype )
    # Resolve the shape, if it's a dynamic shape.
    if _is_dynamic_shape( sd_shape, cls ):
        dyn_shape = _resolve_dynamic_shape( sd_shape, f_name, cls=cls, ref=ref )
        #dyn_shape = _astuple( dyn_shape )
        # Merge the resolved shape.
        if is_dtype_dc:
            sd_shape = dyn_shape
        else:
            sd_dtype = _np_dtype( (sd_dtype, dyn_shape) )
            sd_shape = _resolve_shape( sd_dtype )
    # Cast shape as a tuple.
    sd_shape = _astuple( sd_shape )
    # Remove all `None` values.
    sd_shape = _asarray( sd_shape, dtype=object )
    sd_shape = tuple( sd_shape[sd_shape != _asarray(None)] )
    sd_shape_size: int = _shape_size( sd_shape )
    has_shape = bool( sd_shape )
    is_zero_shaped = bool( has_shape and not sd_shape_size )
    # A dtype with no shape shouid be treated as 1-len array.
    sd_shape_size = sd_shape_size if (is_zero_shaped or has_shape) else 1
    is_zero_ndim = bool( not has_shape and sd_shape_size == 1 )
    # Numpy dataclasses.
    if is_dtype_dc:
        if is_zero_shaped:
            # numpy.asarray due to it has shape, which is ndim > 0.
            f_value = _asarray( [], dtype=sd_dtype )
        else:
            buffer, f_value = __read_dataclass(
                buffer
                , cls=sd_dtype
                , count=sd_shape_size
                , struct_dtype_kwargs=struct_dtype_kwargs
            )
            # Only single item.
            if is_zero_ndim:
                f_value = f_value[0]
            # Apply its own shape.
            elif has_shape:
                f_value = f_value.reshape( sd_shape )
    # Numpy-alikes.
    else:
        if byteorder:
            sd_dtype = _dtype_newbyteorder( sd_dtype, byteorder, ignore_non_native )
        if is_zero_shaped:
            # numpy.asarray due to it has shape, which is ndim > 0.
            f_value = _asarray( [], dtype=_resolve_base(sd_dtype) )
        else:
            buffer, f_value = __read( buffer, dtype=sd_dtype, count=1 )
            f_value = f_value[0]
    f_value = _as_field_type( f_value, f_type, dtype=sd_dtype )
    return (buffer, f_name, f_value,)


def _as_field_type (
    value: _typing.Any
    , field_type: type[_T]
    , *
    , dtype: _DTypeLike = _MISSING
) -> _T:
    if dtype is _MISSING:
        dtype = field_type
    if type(value) is field_type:
        return value
    # Default numpy dtype.
    if field_type is None:
        return _DefaultDType( value )
    # NDArray generics.
    elif field_type is _np_ndarray:
        return _asarray( value, dtype=dtype )
    # Dtype instances.
    elif _is_subclass_or_instance( field_type, (_np_dtype, _np_dtype_generic,) ):
        return _asarray( [value], dtype=dtype )[0]
    # Sized arrays.
    elif t_sa := _get_shapedarray_args( field_type ):
        t_cls,*_ = t_sa
        return t_cls( value )
    # Maybe a built-in type.
    return field_type( value )


def _readbuffer (
    buffer: _BufferLike
    , dtype: _DTypeLike[_SCT]
    , count: int = -1
    , offset: int = 0
    , *
    , like: _SupportsArrayFunc|None = None
    , copy: bool|None = None
) -> tuple[_BufferLike, _NDArray[_SCT]]:
    if copy is None:
        copy = True
    outval: _NDArray[_SCT] = _frombuffer( buffer, dtype, count, offset, like=like )
    if copy:
        outval = outval.copy()
    return (buffer[outval.nbytes:], outval,)


def _readbuffer_dataclass (
    buffer: _BufferLike
    , cls: _typing.Type[_T_ABCNumpyDataclass_co]
    , count: int = -1
    , *
    , struct_dtype_kwargs: dict[str,_typing.Any]|None = None
) -> tuple[_BufferLike, _NDArray]:
    if struct_dtype_kwargs is None:
        struct_dtype_kwargs = {}
    def __iread ():
        nonlocal buffer
        while True:
            obj = cls.frombuffer( buffer, **struct_dtype_kwargs )
            buffer = buffer[obj.nbytes:]
            yield obj
    outval = _fromiter( __iread(), dtype=cls, count=count )
    return (buffer, outval,)


def _readfile (
    fp: _typing.BinaryIO
    , dtype: _np_dtype
    , count: int = -1
    , offset: int = 0
    , *
    , like: _SupportsArrayFunc|None = None
    , copy: bool|None = None
) -> tuple[_typing.BinaryIO, _NDArray[_SCT]]:
    nbytes: int = dtype.itemsize
    if count < 0:
        read_data = fp.read()
    else:
        nbytes *= count
        read_data = fp.read( nbytes )
    return (fp, _readbuffer(read_data, dtype, count, offset, like=like, copy=copy)[1],)


def _readfile_dataclass (
    fp: _typing.BinaryIO
    , cls: _typing.Type[_T_ABCNumpyDataclass_co]
    , count: int = -1
    , *
    , struct_dtype_kwargs: dict[str,_typing.Any]|None = None
) -> tuple[_typing.BinaryIO, _NDArray]:
    if struct_dtype_kwargs is None:
        struct_dtype_kwargs = {}
    def __iread ():
        while True:
            yield cls.fromfile( fp, **struct_dtype_kwargs )
    outval = _fromiter( __iread(), dtype=cls, count=count )
    return (fp, outval,)


def _dtype_newbyteorder (
    dtype: _DTypeLike[_SCT]
    , byteorder: _ByteOrder
    , ignore_non_native: bool
) -> _np_dtype[_SCT]:
    dtype = _np_dtype( dtype )
    if ignore_non_native and dtype.byteorder not in _NATIVE_BYTEORDER:
        return dtype
    return dtype.newbyteorder( byteorder )


def _astuple (
    obj
) -> tuple:
    if isinstance( obj, tuple ):
        return obj
    try:
        return tuple( obj )
    except TypeError:
        return (obj,)


def _getref (
    ref
    , key: str
):
    if isinstance( ref, _typing.Mapping ):
        return ref[key]
    return getattr( ref, key )


def _cls_from (
    cls: _typing.Type[_T_ABCNumpyDataclass_co]
    , buffer: _BufferLike
    , *
    , struct_dtype_kwargs: dict[str,_typing.Any]|None = None
) -> _T_ABCNumpyDataclass_co:
    if struct_dtype_kwargs is None:
        struct_dtype_kwargs = {}
    if not numpy_fields( cls ):
        raise ValueError( f"{cls.__name__} has no numpy field" )
    # Handle those manually since the instance is not created yet.
    it_kwargs = struct_dtype_kwargs.copy()
    it_kwargs.update({
        "resolve_dynamic_shape": False,
        "cast_numpy_dataclass": False,
    })
    cls_kwargs: dict[str, _typing.Any] = {}
    for field, struct_dtype in iter_struct_fields(cls, **it_kwargs):
        buffer, f_name, f_value = _parse_field(
            buffer, field, cls, struct_dtype
            , ref=cls_kwargs
            , struct_dtype_kwargs=struct_dtype_kwargs
        )
        cls_kwargs[f_name] = f_value
    return cls( **cls_kwargs )

"""
Numpy typing utility module.
"""
from __future__ import annotations
import typing as _typing
import typing_extensions as _typingext
from numpy import (
    dtype as _dtype
    , ndarray as _ndarray
)
from numpy._typing import (
    _DTypeLike
    , _ShapeLike
    , _ScalarLike_co
)
from .. import (
    _GENERIC_ALIAS_TYPES as _GENERIC_ALIAS_TYPES
    , GenericAliases as _GenericAlias
    , get_shapedarray_args as _get_shapedarray_args
)



ShapeLike = _typing.Union[_ShapeLike, int, str]
"""Shape-like types."""
_ShapeType = _typing.TypeVar("_ShapeType", bound=ShapeLike, covariant=True)
ByteOrder = _typing.Literal["S", "<", ">", "=", "|", "L", "B", "N", "I"]
"""Numpy byte orders."""
# Some array with "not applicable" byte order ("|")
# are actually has effect on byte-order changes.
_NATIVE_BYTEORDER: tuple[str, ...] = ("=", "|",)
_SCALAR_LIKE_TYPES: tuple[_typing.Type, ...] = _typingext.get_args( _ScalarLike_co )


DEFAULT_DTYPE = _dtype(None).type
"""Default `numpy.dtype`."""


def get_ndarray_args (
    tp: _typing.Type[_typing.Union[_GenericAlias, _typing.Any]]
) -> tuple[ShapeLike|None, _DTypeLike]|None:
    """
    Return tuple of parameters in `numpy.ndarray`-alike generics.
    Otherwise, return `None`.
    """
    # Sized arrays.
    if sa_args := _get_shapedarray_args( tp ):
        _, el_len, el_cls = sa_args
        return (el_len, el_cls,)
    # NDArray generics.
    elif isinstance( tp, _GENERIC_ALIAS_TYPES ):
        if tp.__origin__ is not _ndarray:
            return None
        t_args: tuple[_typing.Any, _DTypeLike] = tp.__args__
        t_shape: ShapeLike
        t_dtype: _DTypeLike
        (t_shape, t_dtype,) = t_args
        if t_shape is _typing.Any:
            t_shape = None
        if isinstance( t_dtype, _GENERIC_ALIAS_TYPES ):
            if t_dtype.__origin__ is _dtype:
                t_dtype = t_dtype.__args__[0]
        return (t_shape, t_dtype,)
    return None
